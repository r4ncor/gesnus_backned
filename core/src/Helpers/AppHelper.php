<?php

namespace App\Helpers;

use Symfony\Component\DependencyInjection\ContainerInterface;

class AppHelper
{
    CONST SALT_RANDOM_CODE_LENGTH = 25;
    CONST SALT_RANDOM_CODE_CHARACTERS = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    CONST LANG_ES = 'es';
    CONST LANG_CA = 'ca';


    /**
     * Obtener el contenedor de depencias de la aplicación
     *
     * @return ContainerInterface|null
     */
    public static function getContainerInterface(): ?ContainerInterface{
        global $kernel;

        return $kernel->getContainer();
    }

    /**
     * Obtener ruta base de la aplicación
     *
     * @return string
     */
    public static function getBaseUrl(): string {
        $request = self::getContainerInterface()->get('request_stack')->getCurrentRequest();

        return $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();
    }

    /**
     * Obtener ruta completa especificando un nombre de ruta
     *
     * @param string $routeName
     * @return string
     */
    public static function getFullPathByRouteName(string $routeName): string {
        $baseUrl = self::getBaseUrl();
        $router = self::getContainerInterface()->get('router');
        $routesCollection = $router->getRouteCollection();

        return $baseUrl . $routesCollection->get($routeName)->getPath();
    }
}