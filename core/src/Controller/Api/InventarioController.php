<?php

namespace App\Controller\Api;

use App\Entity\Equipo;
use App\Helpers\AppHelper;
use App\Exceptions\ValidationException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
//use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\Validator\Constraints as Assert;

class InventarioController extends ApiController
{


//    private $equipoRepository;
//
//    public function __construct(Registry $registry)
//    {
//        $this->equipoRepository = $registry->getRepository(Equipo::class);
//    }


    /**
     * @Route("/inventario/search-by-serial-number", name="serial-number" , methods={"GET"})
     *
     * Devuelve todos los equipos por serial_number
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function SearchEquipoBySerialNumber(Request $request)
    {
        $queryParams = $request->query->all();

        $constraints = [
            'serial_number' => [
                new Assert\NotBlank(),
            ],
            'tipus_seu' => [
                new Assert\NotBlank(),
            ],
        ];

        $this->validateDataRequest($queryParams, $constraints);

        $user = $this->getDoctrine()->getManager();
        $entities =  $user->getRepository(Equipo::class)->findEntitiesBySerialNumber($queryParams);

        if(!$entities) {
            $result = [];
        } else {
            $result = $this->getEntitiesBySerialNumber($entities);
        }

        return $this->successResponse($result);
    }


    /**
     * @Route("/inventario/search-by-hostname", name="hostname" , methods={"GET"})
     *
     * Devuelve todos los equipos por hostname
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function SearchEquipoByHostname(Request $request)
    {
        $queryParams = $request->query->all();

        $constraints = [
            'hostname' => [
                new Assert\NotBlank(),
            ],
            'tipus_seu' => [
                new Assert\NotBlank(),
            ],
        ];

        $this->validateDataRequest($queryParams, $constraints);

        $user = $this->getDoctrine()->getManager();
        $entities =  $user->getRepository(Equipo::class)->findEntitiesByHostname($queryParams);

        if(!$entities) {
            $result = [];
        } else {
            $result = $this->getEntitiesByHostname($entities);
        }

        return $this->successResponse($result);
    }

    /**
     * @Route("/inventario/search-by-identificador", name="identificador" , methods={"GET"})
     *
     * Devuelve todos los equipos por identificador
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function SearchEquipoByIdentificador(Request $request)
    {
        $queryParams = $request->query->all();

        $constraints = [
            'identificador' => [
                new Assert\NotBlank(),
            ],
            'tipus_seu' => [
                new Assert\NotBlank(),
            ],
        ];

        $this->validateDataRequest($queryParams, $constraints);

        $user = $this->getDoctrine()->getManager();
        $entities =  $user->getRepository(Equipo::class)->findEntitiesByIdentificador($queryParams);

        if(!$entities) {
            $result = [];
        } else {
            $result = $this->getEntitiesByIdentificador($entities);
        }

        return $this->successResponse($result);
    }

    public function getEntitiesBySerialNumber($entities){

        foreach ($entities as $entity){
            $data = [
                'id' => $entity->getId(),
                'serial_number' => $entity->getSerialNumber()
            ];
            $realEntities[] = $data;
        }

        return $realEntities;
    }

    public function getEntitiesByHostname($entities){

        foreach ($entities as $entity){
            $data = [
                'id' => $entity->getId(),
                'hostname' => $entity->getHostname()
            ];
            $realEntities[] = $data;
        }

        return $realEntities;
    }

    public function getEntitiesByIdentificador($entities){

        foreach ($entities as $entity){
            $data = [
                'id' => $entity->getId(),
                'identificador' => $entity->getIdentificador(),
                'hostname' => $entity->getHostname(),
                'identificador_arquitectura' => $entity->getIdentificador(),
            ];
            $realEntities[] = $data;
        }

        return $realEntities;
    }
}
