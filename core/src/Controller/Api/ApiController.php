<?php

namespace App\Controller\Api;

use App\Traits\ApiRequestValidation;
use App\Traits\ApiResponse;
//use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\AbstractFOSRestController;



class ApiController extends AbstractFOSRestController
{
    use ApiResponse, ApiRequestValidation;


}
