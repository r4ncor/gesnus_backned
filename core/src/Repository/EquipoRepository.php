<?php

namespace App\Repository;

use App\Entity\Equipo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\Expr\Join;

/**
 * @method Equipo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Equipo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Equipo[]    findAll()
 * @method Equipo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EquipoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Equipo::class);
    }

    /**
     * Encuentra todos los equipos por serial_number y tipo de sede.
     *
     * @param string $str parametro de busqueda
     *
     * @return array
     */
    public function findEntitiesBySerialNumber($str){


        return  $this->createQueryBuilder('e')
            ->join('e.DadesSeu', 'da')
            ->join('da.tipusSeu', 'ts')
            ->addSelect('e')
            ->andWhere('e.serialNumber like :str AND ts.tipus = :tipus')
            ->setParameter('str', $str['serial_number'].'%')
            ->setParameter('tipus', $str['tipus_seu'])
            ->setMaxResults(6)
            ->getQuery()->getResult();
    }


    /**
     * Encuentra todos los equipos por hostname y tipo de sede.
     *
     * @param string $str parametro de busqueda
     *
     * @return array
     */
    public function findEntitiesByHostname($str){


        return  $this->createQueryBuilder('e')
            ->join('e.DadesSeu', 'da')
            ->join('da.tipusSeu', 'ts')
            ->addSelect('e')
            ->andWhere('e.hostname like :str AND ts.tipus = :tipus')
            ->setParameter('str', '%'.$str['hostname'].'%')
            ->setParameter('tipus', $str['tipus_seu'])
            ->setMaxResults(6)
            ->getQuery()->getResult();
    }

    /**
     * Encuentra todos los equipos por identificadors y tipo de sede.
     *
     * @param string $str parametro de busqueda
     *
     * @return array
     */
    public function findEntitiesByIdentificador($str){


        return  $this->createQueryBuilder('e')
            ->join('e.DadesSeu', 'da')
            ->join('da.tipusSeu', 'ts')
            ->Join('e.idEstat', 'i', 'WITH', "i.nom != 'BAIXA'")
            ->addSelect('e')
            ->andWhere('e.identificador like :str AND ts.tipus = :tipus')
            ->setParameter('str', $str['identificador'].'%')
            ->setParameter('tipus', $str['tipus_seu'])
            ->orderBy('e.identificador', 'ASC')
            ->setMaxResults(6)
            ->getQuery()->getResult();
    }

    // /**
    //  * @return Equipo[] Returns an array of Equipo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Equipo
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
