<?php

namespace App\Traits;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

Trait ApiResponse{

    /**
     * Construir respuesta de exito
     *
     * @param null|array $data
     * @param string $message
     * @param int $code
     * @return JsonResponse
     */
    public function successResponse($data = NULL, $message = 'OK', int $code = Response::HTTP_OK){
        return new JsonResponse($data, $code);
    }

    /**
     * Construir respuesta para mostrar el mensaje
     *
     * @param string $message
     * @param int $code
     * @return JsonResponse
     */
    public function showMessageResponse(string $message, int $code = Response::HTTP_OK){
        return $this->successResponse(['msg' => $message], $code);
    }

    /**
     * Construir respuesta de error
     *
     * @param $data
     * @param int $code
     * @return JsonResponse
     */
    public function errorResponse($data, int $code){
        return new JsonResponse($data, $code);
    }
}