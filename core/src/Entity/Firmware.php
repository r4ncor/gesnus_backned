<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Firmware
 *
 * @ORM\Table(name="firmware", uniqueConstraints={@ORM\UniqueConstraint(name="nom", columns={"nom"})}, indexes={@ORM\Index(name="FK_firmware_id_marca", columns={"id_marca"})})
 * @ORM\Entity
 */
class Firmware
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcio", type="text", length=0, nullable=false)
     */
    private $descripcio;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="data_fi", type="date", nullable=true)
     */
    private $dataFi;

    /**
     * @var bool
     *
     * @ORM\Column(name="data_no_publicada", type="boolean", nullable=false)
     */
    private $dataNoPublicada = '0';

    /**
     * @var \Marca
     *
     * @ORM\ManyToOne(targetEntity="Marca")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_marca", referencedColumnName="id")
     * })
     */
    private $idMarca;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescripcio(): ?string
    {
        return $this->descripcio;
    }

    public function setDescripcio(string $descripcio): self
    {
        $this->descripcio = $descripcio;

        return $this;
    }

    public function getDataFi(): ?DateTimeInterface
    {
        return $this->dataFi;
    }

    public function setDataFi(?DateTimeInterface $dataFi): self
    {
        $this->dataFi = $dataFi;

        return $this;
    }

    public function getDataNoPublicada(): ?bool
    {
        return $this->dataNoPublicada;
    }

    public function setDataNoPublicada(bool $dataNoPublicada): self
    {
        $this->dataNoPublicada = $dataNoPublicada;

        return $this;
    }

    public function getIdMarca(): ?Marca
    {
        return $this->idMarca;
    }

    public function setIdMarca(?Marca $idMarca): self
    {
        $this->idMarca = $idMarca;

        return $this;
    }


}
