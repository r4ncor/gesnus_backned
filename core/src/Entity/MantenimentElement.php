<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MantenimentElement
 *
 * @ORM\Table(name="manteniment_element", uniqueConstraints={@ORM\UniqueConstraint(name="unique_codi_marca", columns={"codi_contracte", "id_marca"})}, indexes={@ORM\Index(name="FK_manteniment_element_id_marca", columns={"id_marca"})})
 * @ORM\Entity
 */
class MantenimentElement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codi_contracte", type="string", length=255, nullable=false)
     */
    private $codiContracte;

    /**
     * @var string|null
     *
     * @ORM\Column(name="propietari", type="string", length=255, nullable=true)
     */
    private $propietari;

    /**
     * @var \Marca
     *
     * @ORM\ManyToOne(targetEntity="Marca")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_marca", referencedColumnName="id")
     * })
     */
    private $idMarca;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getCodiContracte(): ?string
    {
        return $this->codiContracte;
    }

    public function setCodiContracte(string $codiContracte): self
    {
        $this->codiContracte = $codiContracte;

        return $this;
    }

    public function getPropietari(): ?string
    {
        return $this->propietari;
    }

    public function setPropietari(?string $propietari): self
    {
        $this->propietari = $propietari;

        return $this;
    }

    public function getIdMarca(): ?Marca
    {
        return $this->idMarca;
    }

    public function setIdMarca(?Marca $idMarca): self
    {
        $this->idMarca = $idMarca;

        return $this;
    }


}
