<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * InterEquipoDadesSeuEstat
 *
 * @ORM\Table(name="inter_equipo_dades_seu_estat", indexes={@ORM\Index(name="FK_inter_equipo_estat_equipo", columns={"id_equipo"}), @ORM\Index(name="FK_inter_equipo_dades_seu_estat_dades_seu", columns={"id_dades_seu"}), @ORM\Index(name="FK_inter_equipo_estat_estat", columns={"id_estat"}), @ORM\Index(name="FK_inter_equipo_estat_usuari", columns={"usuari"})})
 * @ORM\Entity
 */
class InterEquipoDadesSeuEstat
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="data", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $data = 'CURRENT_TIMESTAMP';

    /**
     * @var string|null
     *
     * @ORM\Column(name="observacions", type="text", length=0, nullable=true)
     */
    private $observacions;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hostname", type="string", length=255, nullable=true)
     */
    private $hostname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ip_gestio", type="string", length=255, nullable=true)
     */
    private $ipGestio;

    /**
     * @var \DadesSeu
     *
     * @ORM\ManyToOne(targetEntity="DadesSeu")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_dades_seu", referencedColumnName="id")
     * })
     */
    private $idDadesSeu;

    /**
     * @var \Equipo
     *
     * @ORM\ManyToOne(targetEntity="Equipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_equipo", referencedColumnName="id")
     * })
     */
    private $idEquipo;

    /**
     * @var \Estat
     *
     * @ORM\ManyToOne(targetEntity="Estat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estat", referencedColumnName="id")
     * })
     */
    private $idEstat;

    /**
     * @var \Usuari
     *
     * @ORM\ManyToOne(targetEntity="Usuari")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="usuari", referencedColumnName="id")
     * })
     */
    private $usuari;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getData(): ?DateTimeInterface
    {
        return $this->data;
    }

    public function setData(DateTimeInterface $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getObservacions(): ?string
    {
        return $this->observacions;
    }

    public function setObservacions(?string $observacions): self
    {
        $this->observacions = $observacions;

        return $this;
    }

    public function getHostname(): ?string
    {
        return $this->hostname;
    }

    public function setHostname(?string $hostname): self
    {
        $this->hostname = $hostname;

        return $this;
    }

    public function getIpGestio(): ?string
    {
        return $this->ipGestio;
    }

    public function setIpGestio(?string $ipGestio): self
    {
        $this->ipGestio = $ipGestio;

        return $this;
    }

    public function getIdDadesSeu(): ?DadesSeu
    {
        return $this->idDadesSeu;
    }

    public function setIdDadesSeu(?DadesSeu $idDadesSeu): self
    {
        $this->idDadesSeu = $idDadesSeu;

        return $this;
    }

    public function getIdEquipo(): ?Equipo
    {
        return $this->idEquipo;
    }

    public function setIdEquipo(?Equipo $idEquipo): self
    {
        $this->idEquipo = $idEquipo;

        return $this;
    }

    public function getIdEstat(): ?Estat
    {
        return $this->idEstat;
    }

    public function setIdEstat(?Estat $idEstat): self
    {
        $this->idEstat = $idEstat;

        return $this;
    }

    public function getUsuari(): ?Usuari
    {
        return $this->usuari;
    }

    public function setUsuari(?Usuari $usuari): self
    {
        $this->usuari = $usuari;

        return $this;
    }


}
