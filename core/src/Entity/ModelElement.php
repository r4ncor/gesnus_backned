<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * ModelElement
 *
 * @ORM\Table(name="model_element", uniqueConstraints={@ORM\UniqueConstraint(name="nom_part_number", columns={"nom", "part_number"})}, indexes={@ORM\Index(name="FK_model_marca", columns={"id_marca"})})
 * @ORM\Entity
 */
class ModelElement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="part_number", type="string", length=255, nullable=true)
     */
    private $partNumber;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="data_limit_suport", type="date", nullable=true)
     */
    private $dataLimitSuport;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="data_limit_renovacio", type="date", nullable=true)
     */
    private $dataLimitRenovacio;

    /**
     * @var bool
     *
     * @ORM\Column(name="data_no_publicada", type="boolean", nullable=false)
     */
    private $dataNoPublicada = '0';

    /**
     * @var \Marca
     *
     * @ORM\ManyToOne(targetEntity="Marca")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_marca", referencedColumnName="id")
     * })
     */
    private $idMarca;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPartNumber(): ?string
    {
        return $this->partNumber;
    }

    public function setPartNumber(?string $partNumber): self
    {
        $this->partNumber = $partNumber;

        return $this;
    }

    public function getDataLimitSuport(): ?DateTimeInterface
    {
        return $this->dataLimitSuport;
    }

    public function setDataLimitSuport(?DateTimeInterface $dataLimitSuport): self
    {
        $this->dataLimitSuport = $dataLimitSuport;

        return $this;
    }

    public function getDataLimitRenovacio(): ?DateTimeInterface
    {
        return $this->dataLimitRenovacio;
    }

    public function setDataLimitRenovacio(?DateTimeInterface $dataLimitRenovacio): self
    {
        $this->dataLimitRenovacio = $dataLimitRenovacio;

        return $this;
    }

    public function getDataNoPublicada(): ?bool
    {
        return $this->dataNoPublicada;
    }

    public function setDataNoPublicada(bool $dataNoPublicada): self
    {
        $this->dataNoPublicada = $dataNoPublicada;

        return $this;
    }

    public function getIdMarca(): ?Marca
    {
        return $this->idMarca;
    }

    public function setIdMarca(?Marca $idMarca): self
    {
        $this->idMarca = $idMarca;

        return $this;
    }


}
