<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipusSeu
 *
 * @ORM\Table(name="tipus_seu", uniqueConstraints={@ORM\UniqueConstraint(name="tipus", columns={"tipus"})})
 * @ORM\Entity
 */
class TipusSeu
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tipus", type="string", length=255, nullable=false)
     */
    private $tipus;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getTipus(): ?string
    {
        return $this->tipus;
    }

    public function setTipus(string $tipus): self
    {
        $this->tipus = $tipus;

        return $this;
    }


}
