<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * InterEquipoManteniment
 *
 * @ORM\Table(name="inter_equipo_manteniment", indexes={@ORM\Index(name="FK_inter_equipo_manteniment_equipo", columns={"id_equipo"}), @ORM\Index(name="FK_inter_equipo_manteniment_sla", columns={"id_sla"}), @ORM\Index(name="FK_inter_equipo_manteniment_manteniment", columns={"id_manteniment"}), @ORM\Index(name="FK_inter_equipo_manteniment_usuari", columns={"id_usuari"})})
 * @ORM\Entity
 */
class InterEquipoManteniment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="data_inici", type="datetime", nullable=true)
     */
    private $dataInici;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="data_fi", type="datetime", nullable=true)
     */
    private $dataFi;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="actiu", type="boolean", nullable=true, options={"default"="1"})
     */
    private $actiu = '1';

    /**
     * @var string|null
     *
     * @ORM\Column(name="observacions", type="text", length=0, nullable=true)
     */
    private $observacions;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="data", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $data = 'CURRENT_TIMESTAMP';

    /**
     * @var bool
     *
     * @ORM\Column(name="sla_no_publicat", type="boolean", nullable=false)
     */
    private $slaNoPublicat = '0';

    /**
     * @var \Equipo
     *
     * @ORM\ManyToOne(targetEntity="Equipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_equipo", referencedColumnName="id")
     * })
     */
    private $idEquipo;

    /**
     * @var \Manteniment
     *
     * @ORM\ManyToOne(targetEntity="Manteniment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_manteniment", referencedColumnName="id")
     * })
     */
    private $idManteniment;

    /**
     * @var \Sla
     *
     * @ORM\ManyToOne(targetEntity="Sla")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_sla", referencedColumnName="id")
     * })
     */
    private $idSla;

    /**
     * @var \Usuari
     *
     * @ORM\ManyToOne(targetEntity="Usuari")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuari", referencedColumnName="id")
     * })
     */
    private $idUsuari;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getDataInici(): ?DateTimeInterface
    {
        return $this->dataInici;
    }

    public function setDataInici(?DateTimeInterface $dataInici): self
    {
        $this->dataInici = $dataInici;

        return $this;
    }

    public function getDataFi(): ?DateTimeInterface
    {
        return $this->dataFi;
    }

    public function setDataFi(?DateTimeInterface $dataFi): self
    {
        $this->dataFi = $dataFi;

        return $this;
    }

    public function getActiu(): ?bool
    {
        return $this->actiu;
    }

    public function setActiu(?bool $actiu): self
    {
        $this->actiu = $actiu;

        return $this;
    }

    public function getObservacions(): ?string
    {
        return $this->observacions;
    }

    public function setObservacions(?string $observacions): self
    {
        $this->observacions = $observacions;

        return $this;
    }

    public function getData(): ?DateTimeInterface
    {
        return $this->data;
    }

    public function setData(DateTimeInterface $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getSlaNoPublicat(): ?bool
    {
        return $this->slaNoPublicat;
    }

    public function setSlaNoPublicat(bool $slaNoPublicat): self
    {
        $this->slaNoPublicat = $slaNoPublicat;

        return $this;
    }

    public function getIdEquipo(): ?Equipo
    {
        return $this->idEquipo;
    }

    public function setIdEquipo(?Equipo $idEquipo): self
    {
        $this->idEquipo = $idEquipo;

        return $this;
    }

    public function getIdManteniment(): ?Manteniment
    {
        return $this->idManteniment;
    }

    public function setIdManteniment(?Manteniment $idManteniment): self
    {
        $this->idManteniment = $idManteniment;

        return $this;
    }

    public function getIdSla(): ?Sla
    {
        return $this->idSla;
    }

    public function setIdSla(?Sla $idSla): self
    {
        $this->idSla = $idSla;

        return $this;
    }

    public function getIdUsuari(): ?Usuari
    {
        return $this->idUsuari;
    }

    public function setIdUsuari(?Usuari $idUsuari): self
    {
        $this->idUsuari = $idUsuari;

        return $this;
    }


}
