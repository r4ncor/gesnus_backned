<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * DadesSeu
 *
 * @ORM\Table(name="dades_seu", uniqueConstraints={@ORM\UniqueConstraint(name="uc_scng", columns={"scng"}), @ORM\UniqueConstraint(name="uc_codi_seu", columns={"codi_seu"})}, indexes={@ORM\Index(name="FK_dades_seu_tipus_seu", columns={"tipus_seu"}), @ORM\Index(name="concepte_tarifari", columns={"concepte_tarifari"}), @ORM\Index(name="FK_dades_seu_master_seus", columns={"codi_seu"}), @ORM\Index(name="FK_dades_seu_tipologia_conexio", columns={"id_tipologia_connexio"}), @ORM\Index(name="FK_dades_seu_estat", columns={"id_estat"})})
 * @ORM\Entity
 */
class DadesSeu
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="scng", type="string", length=255, nullable=true)
     */
    private $scng;

    /**
     * @var string|null
     *
     * @ORM\Column(name="persona_contacte", type="string", length=255, nullable=true)
     */
    private $personaContacte;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telefono_contacte", type="string", length=255, nullable=true)
     */
    private $telefonoContacte;

    /**
     * @var string|null
     *
     * @ORM\Column(name="horari_seu", type="string", length=255, nullable=true)
     */
    private $horariSeu;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="data_alta", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $dataAlta = 'CURRENT_TIMESTAMP';

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="data_baixa", type="datetime", nullable=true)
     */
    private $dataBaixa;

    /**
     * @var string|null
     *
     * @ORM\Column(name="provincia", type="string", length=255, nullable=true)
     */
    private $provincia;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pvlan_1", type="string", length=255, nullable=true)
     */
    private $pvlan1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="pvlan_2", type="string", length=255, nullable=true)
     */
    private $pvlan2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ip_publica", type="string", length=255, nullable=true)
     */
    private $ipPublica;

    /**
     * @var \ConcepteTarifariSeu
     *
     * @ORM\ManyToOne(targetEntity="ConcepteTarifariSeu")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="concepte_tarifari", referencedColumnName="id")
     * })
     */
    private $concepteTarifari;

    /**
     * @var \Estat
     *
     * @ORM\ManyToOne(targetEntity="Estat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estat", referencedColumnName="id")
     * })
     */
    private $idEstat;

    /**
     * @var \MasterSeus
     *
     * @ORM\ManyToOne(targetEntity="MasterSeus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="codi_seu", referencedColumnName="codi_seu")
     * })
     */
    private $codiSeu;

    /**
     * @var \TipologiaConnexio
     *
     * @ORM\ManyToOne(targetEntity="TipologiaConnexio")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipologia_connexio", referencedColumnName="id")
     * })
     */
    private $idTipologiaConnexio;

    /**
     * @var \TipusSeu
     *
     * @ORM\ManyToOne(targetEntity="TipusSeu")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipus_seu", referencedColumnName="id")
     * })
     */
    private $tipusSeu;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getScng(): ?string
    {
        return $this->scng;
    }

    public function setScng(?string $scng): self
    {
        $this->scng = $scng;

        return $this;
    }

    public function getPersonaContacte(): ?string
    {
        return $this->personaContacte;
    }

    public function setPersonaContacte(?string $personaContacte): self
    {
        $this->personaContacte = $personaContacte;

        return $this;
    }

    public function getTelefonoContacte(): ?string
    {
        return $this->telefonoContacte;
    }

    public function setTelefonoContacte(?string $telefonoContacte): self
    {
        $this->telefonoContacte = $telefonoContacte;

        return $this;
    }

    public function getHorariSeu(): ?string
    {
        return $this->horariSeu;
    }

    public function setHorariSeu(?string $horariSeu): self
    {
        $this->horariSeu = $horariSeu;

        return $this;
    }

    public function getDataAlta(): ?DateTimeInterface
    {
        return $this->dataAlta;
    }

    public function setDataAlta(DateTimeInterface $dataAlta): self
    {
        $this->dataAlta = $dataAlta;

        return $this;
    }

    public function getDataBaixa(): ?DateTimeInterface
    {
        return $this->dataBaixa;
    }

    public function setDataBaixa(?DateTimeInterface $dataBaixa): self
    {
        $this->dataBaixa = $dataBaixa;

        return $this;
    }

    public function getProvincia(): ?string
    {
        return $this->provincia;
    }

    public function setProvincia(?string $provincia): self
    {
        $this->provincia = $provincia;

        return $this;
    }

    public function getPvlan1(): ?string
    {
        return $this->pvlan1;
    }

    public function setPvlan1(?string $pvlan1): self
    {
        $this->pvlan1 = $pvlan1;

        return $this;
    }

    public function getPvlan2(): ?string
    {
        return $this->pvlan2;
    }

    public function setPvlan2(?string $pvlan2): self
    {
        $this->pvlan2 = $pvlan2;

        return $this;
    }

    public function getIpPublica(): ?string
    {
        return $this->ipPublica;
    }

    public function setIpPublica(?string $ipPublica): self
    {
        $this->ipPublica = $ipPublica;

        return $this;
    }

    public function getConcepteTarifari(): ?ConcepteTarifariSeu
    {
        return $this->concepteTarifari;
    }

    public function setConcepteTarifari(?ConcepteTarifariSeu $concepteTarifari): self
    {
        $this->concepteTarifari = $concepteTarifari;

        return $this;
    }

    public function getIdEstat(): ?Estat
    {
        return $this->idEstat;
    }

    public function setIdEstat(?Estat $idEstat): self
    {
        $this->idEstat = $idEstat;

        return $this;
    }

    public function getCodiSeu(): ?MasterSeus
    {
        return $this->codiSeu;
    }

    public function setCodiSeu(?MasterSeus $codiSeu): self
    {
        $this->codiSeu = $codiSeu;

        return $this;
    }

    public function getIdTipologiaConnexio(): ?TipologiaConnexio
    {
        return $this->idTipologiaConnexio;
    }

    public function setIdTipologiaConnexio(?TipologiaConnexio $idTipologiaConnexio): self
    {
        $this->idTipologiaConnexio = $idTipologiaConnexio;

        return $this;
    }

    public function getTipusSeu(): ?TipusSeu
    {
        return $this->tipusSeu;
    }

    public function setTipusSeu(?TipusSeu $tipusSeu): self
    {
        $this->tipusSeu = $tipusSeu;

        return $this;
    }


}
