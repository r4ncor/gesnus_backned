<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InterUsuariRolPermiso
 *
 * @ORM\Table(name="inter_usuari_rol_permiso", uniqueConstraints={@ORM\UniqueConstraint(name="uc_id_usuari_id_rol", columns={"id_usuari", "id_rol"})}, indexes={@ORM\Index(name="FK_inter_usuarios_roles_permisos_roles", columns={"id_rol"}), @ORM\Index(name="FK_inter_usuarios_roles_permisos_usuari", columns={"id_usuari"}), @ORM\Index(name="FK_inter_usuarios_roles_permisos_permisos", columns={"id_permiso"})})
 * @ORM\Entity
 */
class InterUsuariRolPermiso
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Permiso
     *
     * @ORM\ManyToOne(targetEntity="Permiso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_permiso", referencedColumnName="id")
     * })
     */
    private $idPermiso;

    /**
     * @var \Rol
     *
     * @ORM\ManyToOne(targetEntity="Rol")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_rol", referencedColumnName="id")
     * })
     */
    private $idRol;

    /**
     * @var \Usuari
     *
     * @ORM\ManyToOne(targetEntity="Usuari")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuari", referencedColumnName="id")
     * })
     */
    private $idUsuari;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getIdPermiso(): ?Permiso
    {
        return $this->idPermiso;
    }

    public function setIdPermiso(?Permiso $idPermiso): self
    {
        $this->idPermiso = $idPermiso;

        return $this;
    }

    public function getIdRol(): ?Rol
    {
        return $this->idRol;
    }

    public function setIdRol(?Rol $idRol): self
    {
        $this->idRol = $idRol;

        return $this;
    }

    public function getIdUsuari(): ?Usuari
    {
        return $this->idUsuari;
    }

    public function setIdUsuari(?Usuari $idUsuari): self
    {
        $this->idUsuari = $idUsuari;

        return $this;
    }


}
