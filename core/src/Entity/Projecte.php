<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Projecte
 *
 * @ORM\Table(name="projecte", uniqueConstraints={@ORM\UniqueConstraint(name="uc_codi", columns={"codi"})}, indexes={@ORM\Index(name="FK2_projecte_usuari", columns={"tecnic_referencia"}), @ORM\Index(name="FK3_projecte_usuari", columns={"arquitecte_referencia"}), @ORM\Index(name="FK_projecte_usuari", columns={"project_manager"})})
 * @ORM\Entity
 */
class Projecte
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="codi", type="string", length=255, nullable=false)
     */
    private $codi;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcio", type="string", length=255, nullable=true)
     */
    private $descripcio;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="data_inici_prevista", type="date", nullable=true)
     */
    private $dataIniciPrevista;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="data_fi_prevista", type="date", nullable=true)
     */
    private $dataFiPrevista;

    /**
     * @var \Usuari
     *
     * @ORM\ManyToOne(targetEntity="Usuari")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tecnic_referencia", referencedColumnName="id")
     * })
     */
    private $tecnicReferencia;

    /**
     * @var \Usuari
     *
     * @ORM\ManyToOne(targetEntity="Usuari")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="arquitecte_referencia", referencedColumnName="id")
     * })
     */
    private $arquitecteReferencia;

    /**
     * @var \Usuari
     *
     * @ORM\ManyToOne(targetEntity="Usuari")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="project_manager", referencedColumnName="id")
     * })
     */
    private $projectManager;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getCodi(): ?string
    {
        return $this->codi;
    }

    public function setCodi(string $codi): self
    {
        $this->codi = $codi;

        return $this;
    }

    public function getDescripcio(): ?string
    {
        return $this->descripcio;
    }

    public function setDescripcio(?string $descripcio): self
    {
        $this->descripcio = $descripcio;

        return $this;
    }

    public function getDataIniciPrevista(): ?DateTimeInterface
    {
        return $this->dataIniciPrevista;
    }

    public function setDataIniciPrevista(?DateTimeInterface $dataIniciPrevista): self
    {
        $this->dataIniciPrevista = $dataIniciPrevista;

        return $this;
    }

    public function getDataFiPrevista(): ?DateTimeInterface
    {
        return $this->dataFiPrevista;
    }

    public function setDataFiPrevista(?DateTimeInterface $dataFiPrevista): self
    {
        $this->dataFiPrevista = $dataFiPrevista;

        return $this;
    }

    public function getTecnicReferencia(): ?Usuari
    {
        return $this->tecnicReferencia;
    }

    public function setTecnicReferencia(?Usuari $tecnicReferencia): self
    {
        $this->tecnicReferencia = $tecnicReferencia;

        return $this;
    }

    public function getArquitecteReferencia(): ?Usuari
    {
        return $this->arquitecteReferencia;
    }

    public function setArquitecteReferencia(?Usuari $arquitecteReferencia): self
    {
        $this->arquitecteReferencia = $arquitecteReferencia;

        return $this;
    }

    public function getProjectManager(): ?Usuari
    {
        return $this->projectManager;
    }

    public function setProjectManager(?Usuari $projectManager): self
    {
        $this->projectManager = $projectManager;

        return $this;
    }


}
