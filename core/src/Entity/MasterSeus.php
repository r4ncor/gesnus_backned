<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * MasterSeus
 *
 * @ORM\Table(name="master_seus")
 * @ORM\Entity
 */
class MasterSeus
{
    /**
     * @var string
     *
     * @ORM\Column(name="codi_seu", type="string", length=255, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $codiSeu;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_seu", type="text", length=0, nullable=false)
     */
    private $nomSeu;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codi_centre", type="string", length=255, nullable=true)
     */
    private $codiCentre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tipus_seu", type="text", length=0, nullable=true)
     */
    private $tipusSeu;

    /**
     * @var string
     *
     * @ORM\Column(name="municipi", type="text", length=0, nullable=false)
     */
    private $municipi;

    /**
     * @var string
     *
     * @ORM\Column(name="adreca", type="text", length=0, nullable=false)
     */
    private $adreca;

    /**
     * @var string|null
     *
     * @ORM\Column(name="complement", type="text", length=0, nullable=true)
     */
    private $complement;

    /**
     * @var string|null
     *
     * @ORM\Column(name="organitzacio", type="text", length=0, nullable=true)
     */
    private $organitzacio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="departament", type="text", length=0, nullable=true)
     */
    private $departament;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observacions", type="text", length=0, nullable=true)
     */
    private $observacions;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codi_adreca", type="string", length=255, nullable=true)
     */
    private $codiAdreca;

    /**
     * @var string|null
     *
     * @ORM\Column(name="codi_organitzacio", type="string", length=255, nullable=true)
     */
    private $codiOrganitzacio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="coordenada_x", type="string", length=255, nullable=true)
     */
    private $coordenadaX;

    /**
     * @var string|null
     *
     * @ORM\Column(name="coordenada_y", type="string", length=255, nullable=true)
     */
    private $coordenadaY;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="data_alta", type="date", nullable=true)
     */
    private $dataAlta;

    /**
     * @var bool
     *
     * @ORM\Column(name="cpd", type="boolean", nullable=false)
     */
    private $cpd = '0';

    /**
     * @var bool
     *
     * @ORM\Column(name="magatzem", type="boolean", nullable=false)
     */
    private $magatzem = '0';

    public function getCodiSeu(): ?string
    {
        return $this->codiSeu;
    }

    public function getNomSeu(): ?string
    {
        return $this->nomSeu;
    }

    public function setNomSeu(string $nomSeu): self
    {
        $this->nomSeu = $nomSeu;

        return $this;
    }

    public function getCodiCentre(): ?string
    {
        return $this->codiCentre;
    }

    public function setCodiCentre(?string $codiCentre): self
    {
        $this->codiCentre = $codiCentre;

        return $this;
    }

    public function getTipusSeu(): ?string
    {
        return $this->tipusSeu;
    }

    public function setTipusSeu(?string $tipusSeu): self
    {
        $this->tipusSeu = $tipusSeu;

        return $this;
    }

    public function getMunicipi(): ?string
    {
        return $this->municipi;
    }

    public function setMunicipi(string $municipi): self
    {
        $this->municipi = $municipi;

        return $this;
    }

    public function getAdreca(): ?string
    {
        return $this->adreca;
    }

    public function setAdreca(string $adreca): self
    {
        $this->adreca = $adreca;

        return $this;
    }

    public function getComplement(): ?string
    {
        return $this->complement;
    }

    public function setComplement(?string $complement): self
    {
        $this->complement = $complement;

        return $this;
    }

    public function getOrganitzacio(): ?string
    {
        return $this->organitzacio;
    }

    public function setOrganitzacio(?string $organitzacio): self
    {
        $this->organitzacio = $organitzacio;

        return $this;
    }

    public function getDepartament(): ?string
    {
        return $this->departament;
    }

    public function setDepartament(?string $departament): self
    {
        $this->departament = $departament;

        return $this;
    }

    public function getObservacions(): ?string
    {
        return $this->observacions;
    }

    public function setObservacions(?string $observacions): self
    {
        $this->observacions = $observacions;

        return $this;
    }

    public function getCodiAdreca(): ?string
    {
        return $this->codiAdreca;
    }

    public function setCodiAdreca(?string $codiAdreca): self
    {
        $this->codiAdreca = $codiAdreca;

        return $this;
    }

    public function getCodiOrganitzacio(): ?string
    {
        return $this->codiOrganitzacio;
    }

    public function setCodiOrganitzacio(?string $codiOrganitzacio): self
    {
        $this->codiOrganitzacio = $codiOrganitzacio;

        return $this;
    }

    public function getCoordenadaX(): ?string
    {
        return $this->coordenadaX;
    }

    public function setCoordenadaX(?string $coordenadaX): self
    {
        $this->coordenadaX = $coordenadaX;

        return $this;
    }

    public function getCoordenadaY(): ?string
    {
        return $this->coordenadaY;
    }

    public function setCoordenadaY(?string $coordenadaY): self
    {
        $this->coordenadaY = $coordenadaY;

        return $this;
    }

    public function getDataAlta(): ?DateTimeInterface
    {
        return $this->dataAlta;
    }

    public function setDataAlta(?DateTimeInterface $dataAlta): self
    {
        $this->dataAlta = $dataAlta;

        return $this;
    }

    public function getCpd(): ?bool
    {
        return $this->cpd;
    }

    public function setCpd(bool $cpd): self
    {
        $this->cpd = $cpd;

        return $this;
    }

    public function getMagatzem(): ?bool
    {
        return $this->magatzem;
    }

    public function setMagatzem(bool $magatzem): self
    {
        $this->magatzem = $magatzem;

        return $this;
    }


}
