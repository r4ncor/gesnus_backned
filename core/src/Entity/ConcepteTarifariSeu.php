<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ConcepteTarifariSeu
 *
 * @ORM\Table(name="concepte_tarifari_seu", uniqueConstraints={@ORM\UniqueConstraint(name="id_servei", columns={"id_servei"})})
 * @ORM\Entity
 */
class ConcepteTarifariSeu
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="id_servei", type="string", length=255, nullable=false)
     */
    private $idServei;

    /**
     * @var string
     *
     * @ORM\Column(name="tipologia_servei", type="string", length=255, nullable=false)
     */
    private $tipologiaServei;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="tipus", type="string", length=255, nullable=false)
     */
    private $tipus;

    /**
     * @var float
     *
     * @ORM\Column(name="preu_unitari", type="float", precision=10, scale=0, nullable=false)
     */
    private $preuUnitari;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getIdServei(): ?string
    {
        return $this->idServei;
    }

    public function setIdServei(string $idServei): self
    {
        $this->idServei = $idServei;

        return $this;
    }

    public function getTipologiaServei(): ?string
    {
        return $this->tipologiaServei;
    }

    public function setTipologiaServei(string $tipologiaServei): self
    {
        $this->tipologiaServei = $tipologiaServei;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getTipus(): ?string
    {
        return $this->tipus;
    }

    public function setTipus(string $tipus): self
    {
        $this->tipus = $tipus;

        return $this;
    }

    public function getPreuUnitari(): ?float
    {
        return $this->preuUnitari;
    }

    public function setPreuUnitari(float $preuUnitari): self
    {
        $this->preuUnitari = $preuUnitari;

        return $this;
    }


}
