<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * InterEquipoHostgroupCheck
 *
 * @ORM\Table(name="inter_equipo_hostgroup_check", uniqueConstraints={@ORM\UniqueConstraint(name="uc_id_equipo_id_hostgroup_id_check", columns={"id_equipo", "id_hostgroup", "id_check"})}, indexes={@ORM\Index(name="FK_inter_equipo_hostgroup_equipo", columns={"id_equipo"}), @ORM\Index(name="FK_inter_equipo_hostgroup_hostgroup", columns={"id_hostgroup"}), @ORM\Index(name="FK_inter_equipo_hostgroup_usuari", columns={"id_usuari"}), @ORM\Index(name="FK_inter_equipo_hostgroup_check", columns={"id_check"})})
 * @ORM\Entity
 */
class InterEquipoHostgroupCheck
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="warning", type="bigint", nullable=true)
     */
    private $warning;

    /**
     * @var int|null
     *
     * @ORM\Column(name="critical", type="bigint", nullable=true)
     */
    private $critical;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="data", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $data = 'CURRENT_TIMESTAMP';

    /**
     * @var \Check
     *
     * @ORM\ManyToOne(targetEntity="Check")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_check", referencedColumnName="id")
     * })
     */
    private $idCheck;

    /**
     * @var \Equipo
     *
     * @ORM\ManyToOne(targetEntity="Equipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_equipo", referencedColumnName="id")
     * })
     */
    private $idEquipo;

    /**
     * @var \Hostgroup
     *
     * @ORM\ManyToOne(targetEntity="Hostgroup")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_hostgroup", referencedColumnName="id")
     * })
     */
    private $idHostgroup;

    /**
     * @var \Usuari
     *
     * @ORM\ManyToOne(targetEntity="Usuari")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuari", referencedColumnName="id")
     * })
     */
    private $idUsuari;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getWarning(): ?string
    {
        return $this->warning;
    }

    public function setWarning(?string $warning): self
    {
        $this->warning = $warning;

        return $this;
    }

    public function getCritical(): ?string
    {
        return $this->critical;
    }

    public function setCritical(?string $critical): self
    {
        $this->critical = $critical;

        return $this;
    }

    public function getData(): ?DateTimeInterface
    {
        return $this->data;
    }

    public function setData(DateTimeInterface $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getIdCheck(): ?Check
    {
        return $this->idCheck;
    }

    public function setIdCheck(?Check $idCheck): self
    {
        $this->idCheck = $idCheck;

        return $this;
    }

    public function getIdEquipo(): ?Equipo
    {
        return $this->idEquipo;
    }

    public function setIdEquipo(?Equipo $idEquipo): self
    {
        $this->idEquipo = $idEquipo;

        return $this;
    }

    public function getIdHostgroup(): ?Hostgroup
    {
        return $this->idHostgroup;
    }

    public function setIdHostgroup(?Hostgroup $idHostgroup): self
    {
        $this->idHostgroup = $idHostgroup;

        return $this;
    }

    public function getIdUsuari(): ?Usuari
    {
        return $this->idUsuari;
    }

    public function setIdUsuari(?Usuari $idUsuari): self
    {
        $this->idUsuari = $idUsuari;

        return $this;
    }


}
