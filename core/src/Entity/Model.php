<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Model
 *
 * @ORM\Table(name="model", uniqueConstraints={@ORM\UniqueConstraint(name="uc_part_number", columns={"part_number"})}, indexes={@ORM\Index(name="FK_model_marca", columns={"id_marca"})})
 * @ORM\Entity
 */
class Model
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="part_number", type="string", length=255, nullable=true)
     */
    private $partNumber;

    /**
     * @var string|null
     *
     * @ORM\Column(name="serie", type="string", length=255, nullable=true)
     */
    private $serie;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcio", type="string", length=255, nullable=true)
     */
    private $descripcio;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="data_limit_renovacio", type="date", nullable=true)
     */
    private $dataLimitRenovacio;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="data_limit_suport", type="date", nullable=true)
     */
    private $dataLimitSuport;

    /**
     * @var string|null
     *
     * @ORM\Column(name="numero_us", type="string", length=255, nullable=true)
     */
    private $numeroUs;

    /**
     * @var int|null
     *
     * @ORM\Column(name="numero_fonts_alimentacio", type="bigint", nullable=true)
     */
    private $numeroFontsAlimentacio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="firmware_recomanat", type="string", length=255, nullable=true)
     */
    private $firmwareRecomanat;

    /**
     * @var bool
     *
     * @ORM\Column(name="pare_logic", type="boolean", nullable=false)
     */
    private $pareLogic = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="data_no_publicada", type="boolean", nullable=true)
     */
    private $dataNoPublicada = '0';

    /**
     * @var \Marca
     *
     * @ORM\ManyToOne(targetEntity="Marca")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_marca", referencedColumnName="id")
     * })
     */
    private $idMarca;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPartNumber(): ?string
    {
        return $this->partNumber;
    }

    public function setPartNumber(?string $partNumber): self
    {
        $this->partNumber = $partNumber;

        return $this;
    }

    public function getSerie(): ?string
    {
        return $this->serie;
    }

    public function setSerie(?string $serie): self
    {
        $this->serie = $serie;

        return $this;
    }

    public function getDescripcio(): ?string
    {
        return $this->descripcio;
    }

    public function setDescripcio(?string $descripcio): self
    {
        $this->descripcio = $descripcio;

        return $this;
    }

    public function getDataLimitRenovacio(): ?DateTimeInterface
    {
        return $this->dataLimitRenovacio;
    }

    public function setDataLimitRenovacio(?DateTimeInterface $dataLimitRenovacio): self
    {
        $this->dataLimitRenovacio = $dataLimitRenovacio;

        return $this;
    }

    public function getDataLimitSuport(): ?DateTimeInterface
    {
        return $this->dataLimitSuport;
    }

    public function setDataLimitSuport(?DateTimeInterface $dataLimitSuport): self
    {
        $this->dataLimitSuport = $dataLimitSuport;

        return $this;
    }

    public function getNumeroUs(): ?string
    {
        return $this->numeroUs;
    }

    public function setNumeroUs(?string $numeroUs): self
    {
        $this->numeroUs = $numeroUs;

        return $this;
    }

    public function getNumeroFontsAlimentacio(): ?string
    {
        return $this->numeroFontsAlimentacio;
    }

    public function setNumeroFontsAlimentacio(?string $numeroFontsAlimentacio): self
    {
        $this->numeroFontsAlimentacio = $numeroFontsAlimentacio;

        return $this;
    }

    public function getFirmwareRecomanat(): ?string
    {
        return $this->firmwareRecomanat;
    }

    public function setFirmwareRecomanat(?string $firmwareRecomanat): self
    {
        $this->firmwareRecomanat = $firmwareRecomanat;

        return $this;
    }

    public function getPareLogic(): ?bool
    {
        return $this->pareLogic;
    }

    public function setPareLogic(bool $pareLogic): self
    {
        $this->pareLogic = $pareLogic;

        return $this;
    }

    public function getDataNoPublicada(): ?bool
    {
        return $this->dataNoPublicada;
    }

    public function setDataNoPublicada(?bool $dataNoPublicada): self
    {
        $this->dataNoPublicada = $dataNoPublicada;

        return $this;
    }

    public function getIdMarca(): ?Marca
    {
        return $this->idMarca;
    }

    public function setIdMarca(?Marca $idMarca): self
    {
        $this->idMarca = $idMarca;

        return $this;
    }


}
