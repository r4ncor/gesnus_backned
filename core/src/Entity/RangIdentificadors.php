<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RangIdentificadors
 *
 * @ORM\Table(name="rang_identificadors", indexes={@ORM\Index(name="FK_rang_tipus", columns={"id_tipus"})})
 * @ORM\Entity
 */
class RangIdentificadors
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="inici_rang", type="string", length=255, nullable=true)
     */
    private $iniciRang;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fi_rang", type="string", length=255, nullable=true)
     */
    private $fiRang;

    /**
     * @var \TipusSeu
     *
     * @ORM\ManyToOne(targetEntity="TipusSeu")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipus", referencedColumnName="id")
     * })
     */
    private $idTipus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIniciRang(): ?string
    {
        return $this->iniciRang;
    }

    public function setIniciRang(?string $iniciRang): self
    {
        $this->iniciRang = $iniciRang;

        return $this;
    }

    public function getFiRang(): ?string
    {
        return $this->fiRang;
    }

    public function setFiRang(?string $fiRang): self
    {
        $this->fiRang = $fiRang;

        return $this;
    }

    public function getIdTipus(): ?TipusSeu
    {
        return $this->idTipus;
    }

    public function setIdTipus(?TipusSeu $idTipus): self
    {
        $this->idTipus = $idTipus;

        return $this;
    }


}
