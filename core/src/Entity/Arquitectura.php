<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Arquitectura
 *
 * @ORM\Table(name="arquitectura")
 * @ORM\Entity
 */
class Arquitectura
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="identificador", type="string", length=255, nullable=false)
     */
    private $identificador;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="funcio", type="string", length=255, nullable=false)
     */
    private $funcio;

    /**
     * @var string
     *
     * @ORM\Column(name="num_equipos", type="string", length=255, nullable=false)
     */
    private $numEquipos;

    /**
     * @var bool
     *
     * @ORM\Column(name="mateix_cpd", type="boolean", nullable=false)
     */
    private $mateixCpd;

    /**
     * @var bool
     *
     * @ORM\Column(name="fisic", type="boolean", nullable=false, options={"default"="1"})
     */
    private $fisic = '1';

    /**
     * @var bool
     *
     * @ORM\Column(name="logic", type="boolean", nullable=false)
     */
    private $logic = '0';

    /**
     * @var bool
     *
     * @ORM\Column(name="mateix_contenidor", type="boolean", nullable=false)
     */
    private $mateixContenidor = '0';

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getIdentificador(): ?string
    {
        return $this->identificador;
    }

    public function setIdentificador(string $identificador): self
    {
        $this->identificador = $identificador;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getFuncio(): ?string
    {
        return $this->funcio;
    }

    public function setFuncio(string $funcio): self
    {
        $this->funcio = $funcio;

        return $this;
    }

    public function getNumEquipos(): ?string
    {
        return $this->numEquipos;
    }

    public function setNumEquipos(string $numEquipos): self
    {
        $this->numEquipos = $numEquipos;

        return $this;
    }

    public function getMateixCpd(): ?bool
    {
        return $this->mateixCpd;
    }

    public function setMateixCpd(bool $mateixCpd): self
    {
        $this->mateixCpd = $mateixCpd;

        return $this;
    }

    public function getFisic(): ?bool
    {
        return $this->fisic;
    }

    public function setFisic(bool $fisic): self
    {
        $this->fisic = $fisic;

        return $this;
    }

    public function getLogic(): ?bool
    {
        return $this->logic;
    }

    public function setLogic(bool $logic): self
    {
        $this->logic = $logic;

        return $this;
    }

    public function getMateixContenidor(): ?bool
    {
        return $this->mateixContenidor;
    }

    public function setMateixContenidor(bool $mateixContenidor): self
    {
        $this->mateixContenidor = $mateixContenidor;

        return $this;
    }


}
