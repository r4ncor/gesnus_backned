<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * InterDadesSeuTipologiaConnexio
 *
 * @ORM\Table(name="inter_dades_seu_tipologia_connexio", indexes={@ORM\Index(name="FK_inter_dades_seu_tipologia_connexio_usuari", columns={"id_usuari"}), @ORM\Index(name="FK_inter_dades_seu_tipologia_connexio_dades_seu", columns={"id_dades_seu"}), @ORM\Index(name="FK_inter_dades_seu_tipologia_connexio_tipologia_connexio", columns={"id_tipologia_connexio"})})
 * @ORM\Entity
 */
class InterDadesSeuTipologiaConnexio
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="data", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $data = 'CURRENT_TIMESTAMP';

    /**
     * @var \Usuari
     *
     * @ORM\ManyToOne(targetEntity="Usuari")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuari", referencedColumnName="id")
     * })
     */
    private $idUsuari;

    /**
     * @var \DadesSeu
     *
     * @ORM\ManyToOne(targetEntity="DadesSeu")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_dades_seu", referencedColumnName="id")
     * })
     */
    private $idDadesSeu;

    /**
     * @var \TipologiaConnexio
     *
     * @ORM\ManyToOne(targetEntity="TipologiaConnexio")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipologia_connexio", referencedColumnName="id")
     * })
     */
    private $idTipologiaConnexio;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getData(): ?DateTimeInterface
    {
        return $this->data;
    }

    public function setData(DateTimeInterface $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getIdUsuari(): ?Usuari
    {
        return $this->idUsuari;
    }

    public function setIdUsuari(?Usuari $idUsuari): self
    {
        $this->idUsuari = $idUsuari;

        return $this;
    }

    public function getIdDadesSeu(): ?DadesSeu
    {
        return $this->idDadesSeu;
    }

    public function setIdDadesSeu(?DadesSeu $idDadesSeu): self
    {
        $this->idDadesSeu = $idDadesSeu;

        return $this;
    }

    public function getIdTipologiaConnexio(): ?TipologiaConnexio
    {
        return $this->idTipologiaConnexio;
    }

    public function setIdTipologiaConnexio(?TipologiaConnexio $idTipologiaConnexio): self
    {
        $this->idTipologiaConnexio = $idTipologiaConnexio;

        return $this;
    }


}
