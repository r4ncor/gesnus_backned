<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Equipo
 *
 * @ORM\Table(name="equipo", uniqueConstraints={@ORM\UniqueConstraint(name="uc_id_reservat", columns={"id_reservat"})}, indexes={@ORM\Index(name="FK_equipo_dades_seu", columns={"id_dades_seu"}), @ORM\Index(name="FK_equipo_inter_equipo_estat", columns={"id_estat_actual"}), @ORM\Index(name="FK_equipo_atribut_extra", columns={"id_atribut_extra"}), @ORM\Index(name="FK_equipo_funcionalitat", columns={"id_funcionalitat"}), @ORM\Index(name="FK_equipo_reservat", columns={"id_reservat"}), @ORM\Index(name="FK_equipo_building_block", columns={"id_building_block"}), @ORM\Index(name="FK_equipo_equipo_monit", columns={"monit_parent"}), @ORM\Index(name="FK_equipo_firmware", columns={"id_firmware"}), @ORM\Index(name="FK_equipo_projecte", columns={"id_projecte"}), @ORM\Index(name="FK_equipo_model", columns={"id_model"}), @ORM\Index(name="FK_equipo_concepte_tarifari_equip", columns={"concepte_tarifari_equip"}), @ORM\Index(name="FK_equipo_manteniment", columns={"id_manteniment"}), @ORM\Index(name="FK_equipo_arquitectura", columns={"id_arquitectura"}), @ORM\Index(name="FK_equipo_sla", columns={"id_sla_manteniment"}), @ORM\Index(name="FK_equipo_equipo", columns={"parent"}), @ORM\Index(name="FK_equipo_contracte_ctti", columns={"id_contracte_ctti"}), @ORM\Index(name="FK_equipo_community", columns={"id_community"}), @ORM\Index(name="FK_equipo_estat", columns={"id_estat"}), @ORM\Index(name="FK_equipo_monitoritzacio", columns={"id_monitoritzacio"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\EquipoRepository")
 */
class Equipo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="serial_number", type="string", length=255, nullable=true)
     */
    private $serialNumber;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hostname", type="string", length=255, nullable=true)
     */
    private $hostname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hostname_snmp", type="string", length=255, nullable=true)
     */
    private $hostnameSnmp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hostname_active_directory", type="string", length=255, nullable=true)
     */
    private $hostnameActiveDirectory;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcio_equip", type="text", length=0, nullable=true)
     */
    private $descripcioEquip;

    /**
     * @var string|null
     *
     * @ORM\Column(name="atribut_extra", type="string", length=255, nullable=true)
     */
    private $atributExtra;

    /**
     * @var string|null
     *
     * @ORM\Column(name="identificador", type="string", length=255, nullable=true)
     */
    private $identificador;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ip_gestio", type="string", length=255, nullable=true)
     */
    private $ipGestio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ip_secundaria", type="string", length=255, nullable=true)
     */
    private $ipSecundaria;

    /**
     * @var string|null
     *
     * @ORM\Column(name="identificador_arquitectura", type="string", length=255, nullable=true)
     */
    private $identificadorArquitectura;

    /**
     * @var bool
     *
     * @ORM\Column(name="backup", type="boolean", nullable=false)
     */
    private $backup = '0';

    /**
     * @var bool
     *
     * @ORM\Column(name="fisic", type="boolean", nullable=false, options={"default"="1"})
     */
    private $fisic = '1';

    /**
     * @var bool
     *
     * @ORM\Column(name="cdc", type="boolean", nullable=false)
     */
    private $cdc = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="actualizable", type="boolean", nullable=true, options={"default"="1"})
     */
    private $actualizable = '1';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="encriptat", type="boolean", nullable=true)
     */
    private $encriptat = '0';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="parent_monitoritzacio", type="boolean", nullable=true)
     */
    private $parentMonitoritzacio = '0';

    /**
     * @var bool
     *
     * @ORM\Column(name="lab", type="boolean", nullable=false)
     */
    private $lab = '0';

    /**
     * @var bool
     *
     * @ORM\Column(name="sense_projecte", type="boolean", nullable=false)
     */
    private $senseProjecte = '0';

    /**
     * @var bool
     *
     * @ORM\Column(name="sense_contracte", type="boolean", nullable=false)
     */
    private $senseContracte = '0';

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="data_inici_manteniment", type="datetime", nullable=true)
     */
    private $dataIniciManteniment;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="data_fi_manteniment", type="datetime", nullable=true)
     */
    private $dataFiManteniment;

    /**
     * @var \Arquitectura
     *
     * @ORM\ManyToOne(targetEntity="Arquitectura")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_arquitectura", referencedColumnName="id")
     * })
     */
    private $idArquitectura;

    /**
     * @var \AtributExtra
     *
     * @ORM\ManyToOne(targetEntity="AtributExtra")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_atribut_extra", referencedColumnName="id")
     * })
     */
    private $idAtributExtra;

    /**
     * @var \BuildingBlock
     *
     * @ORM\ManyToOne(targetEntity="BuildingBlock")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_building_block", referencedColumnName="id")
     * })
     */
    private $idBuildingBlock;

    /**
     * @var \Community
     *
     * @ORM\ManyToOne(targetEntity="Community")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_community", referencedColumnName="id")
     * })
     */
    private $idCommunity;

    /**
     * @var \ConcepteTarifariEquip
     *
     * @ORM\ManyToOne(targetEntity="ConcepteTarifariEquip")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="concepte_tarifari_equip", referencedColumnName="id")
     * })
     */
    private $concepteTarifariEquip;

    /**
     * @var \ContracteCtti
     *
     * @ORM\ManyToOne(targetEntity="ContracteCtti")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_contracte_ctti", referencedColumnName="id")
     * })
     */
    private $idContracteCtti;

    /**
     * @var \DadesSeu
     *
     * @ORM\ManyToOne(targetEntity="DadesSeu")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_dades_seu", referencedColumnName="id")
     * })
     */
    private $idDadesSeu;


    /**
     * @var \DadesSeu
     *
     * @ORM\ManyToOne(targetEntity="DadesSeu")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_dades_seu", referencedColumnName="id")
     * })
     */
    private $DadesSeu;


    /**
     * @var \Equipo
     *
     * @ORM\ManyToOne(targetEntity="Equipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent", referencedColumnName="id")
     * })
     */
    private $parent;

    /**
     * @var \Equipo
     *
     * @ORM\ManyToOne(targetEntity="Equipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="monit_parent", referencedColumnName="id")
     * })
     */
    private $monitParent;

    /**
     * @var \Estat
     *
     * @ORM\ManyToOne(targetEntity="Estat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estat", referencedColumnName="id")
     * })
     */
    private $idEstat;

    /**
     * @var \Firmware
     *
     * @ORM\ManyToOne(targetEntity="Firmware")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_firmware", referencedColumnName="id")
     * })
     */
    private $idFirmware;

    /**
     * @var \Funcionalitat
     *
     * @ORM\ManyToOne(targetEntity="Funcionalitat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_funcionalitat", referencedColumnName="id")
     * })
     */
    private $idFuncionalitat;

    /**
     * @var \InterEquipoDadesSeuEstat
     *
     * @ORM\ManyToOne(targetEntity="InterEquipoDadesSeuEstat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estat_actual", referencedColumnName="id")
     * })
     */
    private $idEstatActual;

    /**
     * @var \Manteniment
     *
     * @ORM\ManyToOne(targetEntity="Manteniment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_manteniment", referencedColumnName="id")
     * })
     */
    private $idManteniment;

    /**
     * @var \Model
     *
     * @ORM\ManyToOne(targetEntity="Model")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_model", referencedColumnName="id")
     * })
     */
    private $idModel;

    /**
     * @var \Monitoritzacio
     *
     * @ORM\ManyToOne(targetEntity="Monitoritzacio")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_monitoritzacio", referencedColumnName="id")
     * })
     */
    private $idMonitoritzacio;

    /**
     * @var \Projecte
     *
     * @ORM\ManyToOne(targetEntity="Projecte")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_projecte", referencedColumnName="id")
     * })
     */
    private $idProjecte;

    /**
     * @var \Equipo
     *
     * @ORM\ManyToOne(targetEntity="Equipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_reservat", referencedColumnName="id")
     * })
     */
    private $idReservat;

    /**
     * @var \Sla
     *
     * @ORM\ManyToOne(targetEntity="Sla")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_sla_manteniment", referencedColumnName="id")
     * })
     */
    private $idSlaManteniment;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getSerialNumber(): ?string
    {
        return $this->serialNumber;
    }

    public function setSerialNumber(?string $serialNumber): self
    {
        $this->serialNumber = $serialNumber;

        return $this;
    }

    public function getHostname(): ?string
    {
        return $this->hostname;
    }

    public function setHostname(?string $hostname): self
    {
        $this->hostname = $hostname;

        return $this;
    }

    public function getHostnameSnmp(): ?string
    {
        return $this->hostnameSnmp;
    }

    public function setHostnameSnmp(?string $hostnameSnmp): self
    {
        $this->hostnameSnmp = $hostnameSnmp;

        return $this;
    }

    public function getHostnameActiveDirectory(): ?string
    {
        return $this->hostnameActiveDirectory;
    }

    public function setHostnameActiveDirectory(?string $hostnameActiveDirectory): self
    {
        $this->hostnameActiveDirectory = $hostnameActiveDirectory;

        return $this;
    }

    public function getDescripcioEquip(): ?string
    {
        return $this->descripcioEquip;
    }

    public function setDescripcioEquip(?string $descripcioEquip): self
    {
        $this->descripcioEquip = $descripcioEquip;

        return $this;
    }

    public function getAtributExtra(): ?string
    {
        return $this->atributExtra;
    }

    public function setAtributExtra(?string $atributExtra): self
    {
        $this->atributExtra = $atributExtra;

        return $this;
    }

    public function getIdentificador(): ?string
    {
        return $this->identificador;
    }

    public function setIdentificador(?string $identificador): self
    {
        $this->identificador = $identificador;

        return $this;
    }

    public function getIpGestio(): ?string
    {
        return $this->ipGestio;
    }

    public function setIpGestio(?string $ipGestio): self
    {
        $this->ipGestio = $ipGestio;

        return $this;
    }

    public function getIpSecundaria(): ?string
    {
        return $this->ipSecundaria;
    }

    public function setIpSecundaria(?string $ipSecundaria): self
    {
        $this->ipSecundaria = $ipSecundaria;

        return $this;
    }

    public function getIdentificadorArquitectura(): ?string
    {
        return $this->identificadorArquitectura;
    }

    public function setIdentificadorArquitectura(?string $identificadorArquitectura): self
    {
        $this->identificadorArquitectura = $identificadorArquitectura;

        return $this;
    }

    public function getBackup(): ?bool
    {
        return $this->backup;
    }

    public function setBackup(bool $backup): self
    {
        $this->backup = $backup;

        return $this;
    }

    public function getFisic(): ?bool
    {
        return $this->fisic;
    }

    public function setFisic(bool $fisic): self
    {
        $this->fisic = $fisic;

        return $this;
    }

    public function getCdc(): ?bool
    {
        return $this->cdc;
    }

    public function setCdc(bool $cdc): self
    {
        $this->cdc = $cdc;

        return $this;
    }

    public function getActualizable(): ?bool
    {
        return $this->actualizable;
    }

    public function setActualizable(?bool $actualizable): self
    {
        $this->actualizable = $actualizable;

        return $this;
    }

    public function getEncriptat(): ?bool
    {
        return $this->encriptat;
    }

    public function setEncriptat(?bool $encriptat): self
    {
        $this->encriptat = $encriptat;

        return $this;
    }

    public function getParentMonitoritzacio(): ?bool
    {
        return $this->parentMonitoritzacio;
    }

    public function setParentMonitoritzacio(?bool $parentMonitoritzacio): self
    {
        $this->parentMonitoritzacio = $parentMonitoritzacio;

        return $this;
    }

    public function getLab(): ?bool
    {
        return $this->lab;
    }

    public function setLab(bool $lab): self
    {
        $this->lab = $lab;

        return $this;
    }

    public function getSenseProjecte(): ?bool
    {
        return $this->senseProjecte;
    }

    public function setSenseProjecte(bool $senseProjecte): self
    {
        $this->senseProjecte = $senseProjecte;

        return $this;
    }

    public function getSenseContracte(): ?bool
    {
        return $this->senseContracte;
    }

    public function setSenseContracte(bool $senseContracte): self
    {
        $this->senseContracte = $senseContracte;

        return $this;
    }

    public function getDataIniciManteniment(): ?DateTimeInterface
    {
        return $this->dataIniciManteniment;
    }

    public function setDataIniciManteniment(?DateTimeInterface $dataIniciManteniment): self
    {
        $this->dataIniciManteniment = $dataIniciManteniment;

        return $this;
    }

    public function getDataFiManteniment(): ?DateTimeInterface
    {
        return $this->dataFiManteniment;
    }

    public function setDataFiManteniment(?DateTimeInterface $dataFiManteniment): self
    {
        $this->dataFiManteniment = $dataFiManteniment;

        return $this;
    }

    public function getIdArquitectura(): ?Arquitectura
    {
        return $this->idArquitectura;
    }

    public function setIdArquitectura(?Arquitectura $idArquitectura): self
    {
        $this->idArquitectura = $idArquitectura;

        return $this;
    }

    public function getIdAtributExtra(): ?AtributExtra
    {
        return $this->idAtributExtra;
    }

    public function setIdAtributExtra(?AtributExtra $idAtributExtra): self
    {
        $this->idAtributExtra = $idAtributExtra;

        return $this;
    }

    public function getIdBuildingBlock(): ?BuildingBlock
    {
        return $this->idBuildingBlock;
    }

    public function setIdBuildingBlock(?BuildingBlock $idBuildingBlock): self
    {
        $this->idBuildingBlock = $idBuildingBlock;

        return $this;
    }

    public function getIdCommunity(): ?Community
    {
        return $this->idCommunity;
    }

    public function setIdCommunity(?Community $idCommunity): self
    {
        $this->idCommunity = $idCommunity;

        return $this;
    }

    public function getConcepteTarifariEquip(): ?ConcepteTarifariEquip
    {
        return $this->concepteTarifariEquip;
    }

    public function setConcepteTarifariEquip(?ConcepteTarifariEquip $concepteTarifariEquip): self
    {
        $this->concepteTarifariEquip = $concepteTarifariEquip;

        return $this;
    }

    public function getIdContracteCtti(): ?ContracteCtti
    {
        return $this->idContracteCtti;
    }

    public function setIdContracteCtti(?ContracteCtti $idContracteCtti): self
    {
        $this->idContracteCtti = $idContracteCtti;

        return $this;
    }

    public function getIdDadesSeu(): ?DadesSeu
    {
        return $this->idDadesSeu;
    }

    public function setIdDadesSeu(?DadesSeu $idDadesSeu): self
    {
        $this->idDadesSeu = $idDadesSeu;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getMonitParent(): ?self
    {
        return $this->monitParent;
    }

    public function setMonitParent(?self $monitParent): self
    {
        $this->monitParent = $monitParent;

        return $this;
    }

    public function getIdEstat(): ?Estat
    {
        return $this->idEstat;
    }

    public function setIdEstat(?Estat $idEstat): self
    {
        $this->idEstat = $idEstat;

        return $this;
    }

    public function getIdFirmware(): ?Firmware
    {
        return $this->idFirmware;
    }

    public function setIdFirmware(?Firmware $idFirmware): self
    {
        $this->idFirmware = $idFirmware;

        return $this;
    }

    public function getIdFuncionalitat(): ?Funcionalitat
    {
        return $this->idFuncionalitat;
    }

    public function setIdFuncionalitat(?Funcionalitat $idFuncionalitat): self
    {
        $this->idFuncionalitat = $idFuncionalitat;

        return $this;
    }

    public function getIdEstatActual(): ?InterEquipoDadesSeuEstat
    {
        return $this->idEstatActual;
    }

    public function setIdEstatActual(?InterEquipoDadesSeuEstat $idEstatActual): self
    {
        $this->idEstatActual = $idEstatActual;

        return $this;
    }

    public function getIdManteniment(): ?Manteniment
    {
        return $this->idManteniment;
    }

    public function setIdManteniment(?Manteniment $idManteniment): self
    {
        $this->idManteniment = $idManteniment;

        return $this;
    }

    public function getIdModel(): ?Model
    {
        return $this->idModel;
    }

    public function setIdModel(?Model $idModel): self
    {
        $this->idModel = $idModel;

        return $this;
    }

    public function getIdMonitoritzacio(): ?Monitoritzacio
    {
        return $this->idMonitoritzacio;
    }

    public function setIdMonitoritzacio(?Monitoritzacio $idMonitoritzacio): self
    {
        $this->idMonitoritzacio = $idMonitoritzacio;

        return $this;
    }

    public function getIdProjecte(): ?Projecte
    {
        return $this->idProjecte;
    }

    public function setIdProjecte(?Projecte $idProjecte): self
    {
        $this->idProjecte = $idProjecte;

        return $this;
    }

    public function getIdReservat(): ?self
    {
        return $this->idReservat;
    }

    public function setIdReservat(?self $idReservat): self
    {
        $this->idReservat = $idReservat;

        return $this;
    }

    public function getIdSlaManteniment(): ?Sla
    {
        return $this->idSlaManteniment;
    }

    public function setIdSlaManteniment(?Sla $idSlaManteniment): self
    {
        $this->idSlaManteniment = $idSlaManteniment;

        return $this;
    }

    public function getDadesSeu(): ?DadesSeu
    {
        return $this->DadesSeu;
    }

    public function setDadesSeu(?DadesSeu $DadesSeu): self
    {
        $this->DadesSeu = $DadesSeu;

        return $this;
    }


}
