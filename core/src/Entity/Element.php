<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Element
 *
 * @ORM\Table(name="element", uniqueConstraints={@ORM\UniqueConstraint(name="uc_nom_serial_number_descripcio_equipo_firmware_tipus_element", columns={"id_model_element", "serial_number", "descripcio", "id_equipo", "id_tipus_element", "id_firmware_element", "indice"})}, indexes={@ORM\Index(name="FK_element_estat", columns={"id_estat"}), @ORM\Index(name="FK_element_manteniment_element", columns={"id_manteniment_element"}), @ORM\Index(name="FK_element_equipo", columns={"id_equipo"}), @ORM\Index(name="FK_element_tipus_element", columns={"id_tipus_element"}), @ORM\Index(name="FK_element_firmware_element", columns={"id_firmware_element"}), @ORM\Index(name="FK_element_parent", columns={"parent"}), @ORM\Index(name="IDX_41405E399FFBE6AD", columns={"id_model_element"})})
 * @ORM\Entity
 */
class Element
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcio", type="string", length=255, nullable=true)
     */
    private $descripcio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="serial_number", type="string", length=255, nullable=true)
     */
    private $serialNumber;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="sense_contracte", type="boolean", nullable=true)
     */
    private $senseContracte = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="indice", type="bigint", nullable=true, options={"comment"="indice del elemento dentro de la maquina"})
     */
    private $indice;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="data_inici_manteniment", type="datetime", nullable=true)
     */
    private $dataIniciManteniment;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="data_fi_manteniment", type="datetime", nullable=true)
     */
    private $dataFiManteniment;

    /**
     * @var \Equipo
     *
     * @ORM\ManyToOne(targetEntity="Equipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_equipo", referencedColumnName="id")
     * })
     */
    private $idEquipo;

    /**
     * @var \Estat
     *
     * @ORM\ManyToOne(targetEntity="Estat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estat", referencedColumnName="id")
     * })
     */
    private $idEstat;

    /**
     * @var \FirmwareElement
     *
     * @ORM\ManyToOne(targetEntity="FirmwareElement")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_firmware_element", referencedColumnName="id")
     * })
     */
    private $idFirmwareElement;

    /**
     * @var \MantenimentElement
     *
     * @ORM\ManyToOne(targetEntity="MantenimentElement")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_manteniment_element", referencedColumnName="id")
     * })
     */
    private $idMantenimentElement;

    /**
     * @var \ModelElement
     *
     * @ORM\ManyToOne(targetEntity="ModelElement")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_model_element", referencedColumnName="id")
     * })
     */
    private $idModelElement;

    /**
     * @var \Element
     *
     * @ORM\ManyToOne(targetEntity="Element")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent", referencedColumnName="id")
     * })
     */
    private $parent;

    /**
     * @var \TipusElement
     *
     * @ORM\ManyToOne(targetEntity="TipusElement")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipus_element", referencedColumnName="id")
     * })
     */
    private $idTipusElement;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getDescripcio(): ?string
    {
        return $this->descripcio;
    }

    public function setDescripcio(?string $descripcio): self
    {
        $this->descripcio = $descripcio;

        return $this;
    }

    public function getSerialNumber(): ?string
    {
        return $this->serialNumber;
    }

    public function setSerialNumber(?string $serialNumber): self
    {
        $this->serialNumber = $serialNumber;

        return $this;
    }

    public function getSenseContracte(): ?bool
    {
        return $this->senseContracte;
    }

    public function setSenseContracte(?bool $senseContracte): self
    {
        $this->senseContracte = $senseContracte;

        return $this;
    }

    public function getIndice(): ?string
    {
        return $this->indice;
    }

    public function setIndice(?string $indice): self
    {
        $this->indice = $indice;

        return $this;
    }

    public function getDataIniciManteniment(): ?DateTimeInterface
    {
        return $this->dataIniciManteniment;
    }

    public function setDataIniciManteniment(?DateTimeInterface $dataIniciManteniment): self
    {
        $this->dataIniciManteniment = $dataIniciManteniment;

        return $this;
    }

    public function getDataFiManteniment(): ?DateTimeInterface
    {
        return $this->dataFiManteniment;
    }

    public function setDataFiManteniment(?DateTimeInterface $dataFiManteniment): self
    {
        $this->dataFiManteniment = $dataFiManteniment;

        return $this;
    }

    public function getIdEquipo(): ?Equipo
    {
        return $this->idEquipo;
    }

    public function setIdEquipo(?Equipo $idEquipo): self
    {
        $this->idEquipo = $idEquipo;

        return $this;
    }

    public function getIdEstat(): ?Estat
    {
        return $this->idEstat;
    }

    public function setIdEstat(?Estat $idEstat): self
    {
        $this->idEstat = $idEstat;

        return $this;
    }

    public function getIdFirmwareElement(): ?FirmwareElement
    {
        return $this->idFirmwareElement;
    }

    public function setIdFirmwareElement(?FirmwareElement $idFirmwareElement): self
    {
        $this->idFirmwareElement = $idFirmwareElement;

        return $this;
    }

    public function getIdMantenimentElement(): ?MantenimentElement
    {
        return $this->idMantenimentElement;
    }

    public function setIdMantenimentElement(?MantenimentElement $idMantenimentElement): self
    {
        $this->idMantenimentElement = $idMantenimentElement;

        return $this;
    }

    public function getIdModelElement(): ?ModelElement
    {
        return $this->idModelElement;
    }

    public function setIdModelElement(?ModelElement $idModelElement): self
    {
        $this->idModelElement = $idModelElement;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getIdTipusElement(): ?TipusElement
    {
        return $this->idTipusElement;
    }

    public function setIdTipusElement(?TipusElement $idTipusElement): self
    {
        $this->idTipusElement = $idTipusElement;

        return $this;
    }


}
