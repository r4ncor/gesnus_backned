<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Funcionalitat
 *
 * @ORM\Table(name="funcionalitat", uniqueConstraints={@ORM\UniqueConstraint(name="uc_identificador", columns={"identificador"}), @ORM\UniqueConstraint(name="uc_nom", columns={"nom"})}, indexes={@ORM\Index(name="FK_funcionalitat_tipologia", columns={"id_tipologia"})})
 * @ORM\Entity
 */
class Funcionalitat
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="identificador", type="string", length=255, nullable=true)
     */
    private $identificador;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcio", type="string", length=255, nullable=true)
     */
    private $descripcio;

    /**
     * @var \Tipologia
     *
     * @ORM\ManyToOne(targetEntity="Tipologia")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipologia", referencedColumnName="id")
     * })
     */
    private $idTipologia;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getIdentificador(): ?string
    {
        return $this->identificador;
    }

    public function setIdentificador(?string $identificador): self
    {
        $this->identificador = $identificador;

        return $this;
    }

    public function getDescripcio(): ?string
    {
        return $this->descripcio;
    }

    public function setDescripcio(?string $descripcio): self
    {
        $this->descripcio = $descripcio;

        return $this;
    }

    public function getIdTipologia(): ?Tipologia
    {
        return $this->idTipologia;
    }

    public function setIdTipologia(?Tipologia $idTipologia): self
    {
        $this->idTipologia = $idTipologia;

        return $this;
    }


}
