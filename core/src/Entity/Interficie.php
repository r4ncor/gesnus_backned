<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Interficie
 *
 * @ORM\Table(name="interficie", uniqueConstraints={@ORM\UniqueConstraint(name="uc_id_equipo_nom_ip", columns={"id_equipo", "nom", "ip"})}, indexes={@ORM\Index(name="FK_interficie_equipo", columns={"id_equipo"})})
 * @ORM\Entity
 */
class Interficie
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcio", type="text", length=0, nullable=true)
     */
    private $descripcio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ip", type="string", length=255, nullable=true)
     */
    private $ip;

    /**
     * @var string|null
     *
     * @ORM\Column(name="mascara", type="string", length=255, nullable=true)
     */
    private $mascara;

    /**
     * @var string|null
     *
     * @ORM\Column(name="xarxa", type="string", length=255, nullable=true)
     */
    private $xarxa;

    /**
     * @var string|null
     *
     * @ORM\Column(name="gateway", type="string", length=255, nullable=true)
     */
    private $gateway;

    /**
     * @var \Equipo
     *
     * @ORM\ManyToOne(targetEntity="Equipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_equipo", referencedColumnName="id")
     * })
     */
    private $idEquipo;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescripcio(): ?string
    {
        return $this->descripcio;
    }

    public function setDescripcio(?string $descripcio): self
    {
        $this->descripcio = $descripcio;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(?string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getMascara(): ?string
    {
        return $this->mascara;
    }

    public function setMascara(?string $mascara): self
    {
        $this->mascara = $mascara;

        return $this;
    }

    public function getXarxa(): ?string
    {
        return $this->xarxa;
    }

    public function setXarxa(?string $xarxa): self
    {
        $this->xarxa = $xarxa;

        return $this;
    }

    public function getGateway(): ?string
    {
        return $this->gateway;
    }

    public function setGateway(?string $gateway): self
    {
        $this->gateway = $gateway;

        return $this;
    }

    public function getIdEquipo(): ?Equipo
    {
        return $this->idEquipo;
    }

    public function setIdEquipo(?Equipo $idEquipo): self
    {
        $this->idEquipo = $idEquipo;

        return $this;
    }


}
