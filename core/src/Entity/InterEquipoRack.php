<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * InterEquipoRack
 *
 * @ORM\Table(name="inter_equipo_rack", indexes={@ORM\Index(name="FK_inter_equipo_rack_rack", columns={"id_rack"}), @ORM\Index(name="FK_inter_equipo_rack_equipo", columns={"id_equipo"}), @ORM\Index(name="FK_inter_equipo_rack_usuari", columns={"id_usuari"})})
 * @ORM\Entity
 */
class InterEquipoRack
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="posicio_u", type="string", length=255, nullable=true)
     */
    private $posicioU;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="data", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $data = 'CURRENT_TIMESTAMP';

    /**
     * @var string|null
     *
     * @ORM\Column(name="observacions", type="text", length=0, nullable=true)
     */
    private $observacions;

    /**
     * @var \Equipo
     *
     * @ORM\ManyToOne(targetEntity="Equipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_equipo", referencedColumnName="id")
     * })
     */
    private $idEquipo;

    /**
     * @var \Rack
     *
     * @ORM\ManyToOne(targetEntity="Rack")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_rack", referencedColumnName="id")
     * })
     */
    private $idRack;

    /**
     * @var \Usuari
     *
     * @ORM\ManyToOne(targetEntity="Usuari")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuari", referencedColumnName="id")
     * })
     */
    private $idUsuari;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getPosicioU(): ?string
    {
        return $this->posicioU;
    }

    public function setPosicioU(?string $posicioU): self
    {
        $this->posicioU = $posicioU;

        return $this;
    }

    public function getData(): ?DateTimeInterface
    {
        return $this->data;
    }

    public function setData(DateTimeInterface $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getObservacions(): ?string
    {
        return $this->observacions;
    }

    public function setObservacions(?string $observacions): self
    {
        $this->observacions = $observacions;

        return $this;
    }

    public function getIdEquipo(): ?Equipo
    {
        return $this->idEquipo;
    }

    public function setIdEquipo(?Equipo $idEquipo): self
    {
        $this->idEquipo = $idEquipo;

        return $this;
    }

    public function getIdRack(): ?Rack
    {
        return $this->idRack;
    }

    public function setIdRack(?Rack $idRack): self
    {
        $this->idRack = $idRack;

        return $this;
    }

    public function getIdUsuari(): ?Usuari
    {
        return $this->idUsuari;
    }

    public function setIdUsuari(?Usuari $idUsuari): self
    {
        $this->idUsuari = $idUsuari;

        return $this;
    }


}
