<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InterInterficieCircuit
 *
 * @ORM\Table(name="inter_interficie_circuit", uniqueConstraints={@ORM\UniqueConstraint(name="id_interficie_id_circuit", columns={"id_interficie", "id_circuit"})}, indexes={@ORM\Index(name="FK_inter_interficie_circuit_circuit", columns={"id_circuit"}), @ORM\Index(name="FK_inter_interficie_circuit_interficie", columns={"id_interficie"})})
 * @ORM\Entity
 */
class InterInterficieCircuit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Circuit
     *
     * @ORM\ManyToOne(targetEntity="Circuit")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_circuit", referencedColumnName="id")
     * })
     */
    private $idCircuit;

    /**
     * @var \Interficie
     *
     * @ORM\ManyToOne(targetEntity="Interficie")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_interficie", referencedColumnName="id")
     * })
     */
    private $idInterficie;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getIdCircuit(): ?Circuit
    {
        return $this->idCircuit;
    }

    public function setIdCircuit(?Circuit $idCircuit): self
    {
        $this->idCircuit = $idCircuit;

        return $this;
    }

    public function getIdInterficie(): ?Interficie
    {
        return $this->idInterficie;
    }

    public function setIdInterficie(?Interficie $idInterficie): self
    {
        $this->idInterficie = $idInterficie;

        return $this;
    }


}
