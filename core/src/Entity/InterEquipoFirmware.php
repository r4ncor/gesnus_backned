<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * InterEquipoFirmware
 *
 * @ORM\Table(name="inter_equipo_firmware", indexes={@ORM\Index(name="FK_inter_equipo_firmware_firmware", columns={"id_firmware"}), @ORM\Index(name="FK_inter_equipo_firmware_equipo", columns={"id_equipo"})})
 * @ORM\Entity
 */
class InterEquipoFirmware
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="data", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $data = 'CURRENT_TIMESTAMP';

    /**
     * @var \Equipo
     *
     * @ORM\ManyToOne(targetEntity="Equipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_equipo", referencedColumnName="id")
     * })
     */
    private $idEquipo;

    /**
     * @var \Firmware
     *
     * @ORM\ManyToOne(targetEntity="Firmware")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_firmware", referencedColumnName="id")
     * })
     */
    private $idFirmware;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getData(): ?DateTimeInterface
    {
        return $this->data;
    }

    public function setData(?DateTimeInterface $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getIdEquipo(): ?Equipo
    {
        return $this->idEquipo;
    }

    public function setIdEquipo(?Equipo $idEquipo): self
    {
        $this->idEquipo = $idEquipo;

        return $this;
    }

    public function getIdFirmware(): ?Firmware
    {
        return $this->idFirmware;
    }

    public function setIdFirmware(?Firmware $idFirmware): self
    {
        $this->idFirmware = $idFirmware;

        return $this;
    }


}
