<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rack
 *
 * @ORM\Table(name="rack", uniqueConstraints={@ORM\UniqueConstraint(name="uc_id_sala_nom", columns={"id_sala", "nom"})}, indexes={@ORM\Index(name="IDX_3DD796A86906181E", columns={"id_sala"})})
 * @ORM\Entity
 */
class Rack
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @var int|null
     *
     * @ORM\Column(name="total_rus", type="integer", nullable=true)
     */
    private $totalRus;

    /**
     * @var \Sala
     *
     * @ORM\ManyToOne(targetEntity="Sala")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_sala", referencedColumnName="id")
     * })
     */
    private $idSala;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getTotalRus(): ?int
    {
        return $this->totalRus;
    }

    public function setTotalRus(?int $totalRus): self
    {
        $this->totalRus = $totalRus;

        return $this;
    }

    public function getIdSala(): ?Sala
    {
        return $this->idSala;
    }

    public function setIdSala(?Sala $idSala): self
    {
        $this->idSala = $idSala;

        return $this;
    }


}
