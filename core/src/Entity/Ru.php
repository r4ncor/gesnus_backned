<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ru
 *
 * @ORM\Table(name="ru", uniqueConstraints={@ORM\UniqueConstraint(name="id_rack_num", columns={"id_rack", "num"})}, indexes={@ORM\Index(name="FK2_ru_equipo", columns={"id_equipo_futuro"}), @ORM\Index(name="FK_ru_estat", columns={"id_estat"}), @ORM\Index(name="FK_ru_equipo", columns={"id_equipo"}), @ORM\Index(name="IDX_7CBF8C38B6F78AAA", columns={"id_rack"})})
 * @ORM\Entity
 */
class Ru
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="num", type="integer", nullable=false)
     */
    private $num;

    /**
     * @var \Equipo
     *
     * @ORM\ManyToOne(targetEntity="Equipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_equipo_futuro", referencedColumnName="id")
     * })
     */
    private $idEquipoFuturo;

    /**
     * @var \Equipo
     *
     * @ORM\ManyToOne(targetEntity="Equipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_equipo", referencedColumnName="id")
     * })
     */
    private $idEquipo;

    /**
     * @var \Estat
     *
     * @ORM\ManyToOne(targetEntity="Estat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estat", referencedColumnName="id")
     * })
     */
    private $idEstat;

    /**
     * @var \Rack
     *
     * @ORM\ManyToOne(targetEntity="Rack")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_rack", referencedColumnName="id")
     * })
     */
    private $idRack;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getNum(): ?int
    {
        return $this->num;
    }

    public function setNum(int $num): self
    {
        $this->num = $num;

        return $this;
    }

    public function getIdEquipoFuturo(): ?Equipo
    {
        return $this->idEquipoFuturo;
    }

    public function setIdEquipoFuturo(?Equipo $idEquipoFuturo): self
    {
        $this->idEquipoFuturo = $idEquipoFuturo;

        return $this;
    }

    public function getIdEquipo(): ?Equipo
    {
        return $this->idEquipo;
    }

    public function setIdEquipo(?Equipo $idEquipo): self
    {
        $this->idEquipo = $idEquipo;

        return $this;
    }

    public function getIdEstat(): ?Estat
    {
        return $this->idEstat;
    }

    public function setIdEstat(?Estat $idEstat): self
    {
        $this->idEstat = $idEstat;

        return $this;
    }

    public function getIdRack(): ?Rack
    {
        return $this->idRack;
    }

    public function setIdRack(?Rack $idRack): self
    {
        $this->idRack = $idRack;

        return $this;
    }


}
