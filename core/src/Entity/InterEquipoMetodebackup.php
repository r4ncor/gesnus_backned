<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InterEquipoMetodebackup
 *
 * @ORM\Table(name="inter_equipo_metodebackup", uniqueConstraints={@ORM\UniqueConstraint(name="uc_equipo_metodebackup", columns={"id_equipo", "id_metodebackup"})}, indexes={@ORM\Index(name="FK_equipo_metodebackup_equipo", columns={"id_equipo"}), @ORM\Index(name="FK_equipo_metodebackup_metode_backup", columns={"id_metodebackup"})})
 * @ORM\Entity
 */
class InterEquipoMetodebackup
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Equipo
     *
     * @ORM\ManyToOne(targetEntity="Equipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_equipo", referencedColumnName="id")
     * })
     */
    private $idEquipo;

    /**
     * @var \MetodeBackup
     *
     * @ORM\ManyToOne(targetEntity="MetodeBackup")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_metodebackup", referencedColumnName="id")
     * })
     */
    private $idMetodebackup;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getIdEquipo(): ?Equipo
    {
        return $this->idEquipo;
    }

    public function setIdEquipo(?Equipo $idEquipo): self
    {
        $this->idEquipo = $idEquipo;

        return $this;
    }

    public function getIdMetodebackup(): ?MetodeBackup
    {
        return $this->idMetodebackup;
    }

    public function setIdMetodebackup(?MetodeBackup $idMetodebackup): self
    {
        $this->idMetodebackup = $idMetodebackup;

        return $this;
    }


}
