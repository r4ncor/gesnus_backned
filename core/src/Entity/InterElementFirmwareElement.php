<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * InterElementFirmwareElement
 *
 * @ORM\Table(name="inter_element_firmware_element", indexes={@ORM\Index(name="FK_inter_element_firmware_usuari", columns={"id_usuari"}), @ORM\Index(name="FK_inter_element_firmware_element", columns={"id_element"}), @ORM\Index(name="FK_inter_element_firmware_firmware_element", columns={"id_firmware"})})
 * @ORM\Entity
 */
class InterElementFirmwareElement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="data", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $data = 'CURRENT_TIMESTAMP';

    /**
     * @var \Element
     *
     * @ORM\ManyToOne(targetEntity="Element")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_element", referencedColumnName="id")
     * })
     */
    private $idElement;

    /**
     * @var \FirmwareElement
     *
     * @ORM\ManyToOne(targetEntity="FirmwareElement")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_firmware", referencedColumnName="id")
     * })
     */
    private $idFirmware;

    /**
     * @var \Usuari
     *
     * @ORM\ManyToOne(targetEntity="Usuari")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuari", referencedColumnName="id")
     * })
     */
    private $idUsuari;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getData(): ?DateTimeInterface
    {
        return $this->data;
    }

    public function setData(DateTimeInterface $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getIdElement(): ?Element
    {
        return $this->idElement;
    }

    public function setIdElement(?Element $idElement): self
    {
        $this->idElement = $idElement;

        return $this;
    }

    public function getIdFirmware(): ?FirmwareElement
    {
        return $this->idFirmware;
    }

    public function setIdFirmware(?FirmwareElement $idFirmware): self
    {
        $this->idFirmware = $idFirmware;

        return $this;
    }

    public function getIdUsuari(): ?Usuari
    {
        return $this->idUsuari;
    }

    public function setIdUsuari(?Usuari $idUsuari): self
    {
        $this->idUsuari = $idUsuari;

        return $this;
    }


}
