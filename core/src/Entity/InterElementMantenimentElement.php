<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * InterElementMantenimentElement
 *
 * @ORM\Table(name="inter_element_manteniment_element", indexes={@ORM\Index(name="FK_inter_element_manteniment_manteniment_element", columns={"id_manteniment_element"}), @ORM\Index(name="FK_inter_element_manteniment_element", columns={"id_element"}), @ORM\Index(name="FK_inter_element_manteniment_usuari", columns={"id_usuari"})})
 * @ORM\Entity
 */
class InterElementMantenimentElement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="actiu", type="boolean", nullable=false, options={"default"="1","comment"="Estat actiu 1 actiu / 0 desactivat"})
     */
    private $actiu = '1';

    /**
     * @var int
     *
     * @ORM\Column(name="id_manteniment_element", type="bigint", nullable=false)
     */
    private $idMantenimentElement;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="data_inici", type="datetime", nullable=true)
     */
    private $dataInici;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="data_fi", type="datetime", nullable=true)
     */
    private $dataFi;

    /**
     * @var bool
     *
     * @ORM\Column(name="id_sla", type="boolean", nullable=false)
     */
    private $idSla;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observacions", type="text", length=0, nullable=true)
     */
    private $observacions;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="data", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $data = 'CURRENT_TIMESTAMP';

    /**
     * @var \Element
     *
     * @ORM\ManyToOne(targetEntity="Element")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_element", referencedColumnName="id")
     * })
     */
    private $idElement;

    /**
     * @var \Usuari
     *
     * @ORM\ManyToOne(targetEntity="Usuari")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuari", referencedColumnName="id")
     * })
     */
    private $idUsuari;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getActiu(): ?bool
    {
        return $this->actiu;
    }

    public function setActiu(bool $actiu): self
    {
        $this->actiu = $actiu;

        return $this;
    }

    public function getIdMantenimentElement(): ?string
    {
        return $this->idMantenimentElement;
    }

    public function setIdMantenimentElement(string $idMantenimentElement): self
    {
        $this->idMantenimentElement = $idMantenimentElement;

        return $this;
    }

    public function getDataInici(): ?DateTimeInterface
    {
        return $this->dataInici;
    }

    public function setDataInici(?DateTimeInterface $dataInici): self
    {
        $this->dataInici = $dataInici;

        return $this;
    }

    public function getDataFi(): ?DateTimeInterface
    {
        return $this->dataFi;
    }

    public function setDataFi(?DateTimeInterface $dataFi): self
    {
        $this->dataFi = $dataFi;

        return $this;
    }

    public function getIdSla(): ?bool
    {
        return $this->idSla;
    }

    public function setIdSla(bool $idSla): self
    {
        $this->idSla = $idSla;

        return $this;
    }

    public function getObservacions(): ?string
    {
        return $this->observacions;
    }

    public function setObservacions(?string $observacions): self
    {
        $this->observacions = $observacions;

        return $this;
    }

    public function getData(): ?DateTimeInterface
    {
        return $this->data;
    }

    public function setData(?DateTimeInterface $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getIdElement(): ?Element
    {
        return $this->idElement;
    }

    public function setIdElement(?Element $idElement): self
    {
        $this->idElement = $idElement;

        return $this;
    }

    public function getIdUsuari(): ?Usuari
    {
        return $this->idUsuari;
    }

    public function setIdUsuari(?Usuari $idUsuari): self
    {
        $this->idUsuari = $idUsuari;

        return $this;
    }


}
