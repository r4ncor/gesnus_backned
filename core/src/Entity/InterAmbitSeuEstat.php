<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * InterAmbitSeuEstat
 *
 * @ORM\Table(name="inter_ambit_seu_estat", indexes={@ORM\Index(name="FK_inter_ambit_seu_estat_estat", columns={"id_estat"}), @ORM\Index(name="FK_inter_ambit_seu_estat_ambit_seu", columns={"id_ambit_seu"}), @ORM\Index(name="FK_inter_ambit_seu_estat_usuari", columns={"id_usuari"})})
 * @ORM\Entity
 */
class InterAmbitSeuEstat
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observacions", type="text", length=0, nullable=true)
     */
    private $observacions;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="data", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $data = 'CURRENT_TIMESTAMP';

    /**
     * @var \AmbitSeu
     *
     * @ORM\ManyToOne(targetEntity="AmbitSeu")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_ambit_seu", referencedColumnName="id")
     * })
     */
    private $idAmbitSeu;

    /**
     * @var \Estat
     *
     * @ORM\ManyToOne(targetEntity="Estat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estat", referencedColumnName="id")
     * })
     */
    private $idEstat;

    /**
     * @var \Usuari
     *
     * @ORM\ManyToOne(targetEntity="Usuari")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuari", referencedColumnName="id")
     * })
     */
    private $idUsuari;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getObservacions(): ?string
    {
        return $this->observacions;
    }

    public function setObservacions(?string $observacions): self
    {
        $this->observacions = $observacions;

        return $this;
    }

    public function getData(): ?DateTimeInterface
    {
        return $this->data;
    }

    public function setData(?DateTimeInterface $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getIdAmbitSeu(): ?AmbitSeu
    {
        return $this->idAmbitSeu;
    }

    public function setIdAmbitSeu(?AmbitSeu $idAmbitSeu): self
    {
        $this->idAmbitSeu = $idAmbitSeu;

        return $this;
    }

    public function getIdEstat(): ?Estat
    {
        return $this->idEstat;
    }

    public function setIdEstat(?Estat $idEstat): self
    {
        $this->idEstat = $idEstat;

        return $this;
    }

    public function getIdUsuari(): ?Usuari
    {
        return $this->idUsuari;
    }

    public function setIdUsuari(?Usuari $idUsuari): self
    {
        $this->idUsuari = $idUsuari;

        return $this;
    }


}
