<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * InterElementEquipoEstat
 *
 * @ORM\Table(name="inter_element_equipo_estat", indexes={@ORM\Index(name="FK_inter_element_estat_estat", columns={"id_estat"}), @ORM\Index(name="FK_inter_element_estat_usuari", columns={"id_usuari"}), @ORM\Index(name="FK_inter_element_estat_element", columns={"id_element"})})
 * @ORM\Entity
 */
class InterElementEquipoEstat
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observacions", type="text", length=0, nullable=true)
     */
    private $observacions;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="data", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $data = 'CURRENT_TIMESTAMP';

    /**
     * @var int
     *
     * @ORM\Column(name="id_equipo", type="bigint", nullable=false)
     */
    private $idEquipo;

    /**
     * @var \Element
     *
     * @ORM\ManyToOne(targetEntity="Element")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_element", referencedColumnName="id")
     * })
     */
    private $idElement;

    /**
     * @var \Estat
     *
     * @ORM\ManyToOne(targetEntity="Estat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estat", referencedColumnName="id")
     * })
     */
    private $idEstat;

    /**
     * @var \Usuari
     *
     * @ORM\ManyToOne(targetEntity="Usuari")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuari", referencedColumnName="id")
     * })
     */
    private $idUsuari;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getObservacions(): ?string
    {
        return $this->observacions;
    }

    public function setObservacions(?string $observacions): self
    {
        $this->observacions = $observacions;

        return $this;
    }

    public function getData(): ?DateTimeInterface
    {
        return $this->data;
    }

    public function setData(DateTimeInterface $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getIdEquipo(): ?string
    {
        return $this->idEquipo;
    }

    public function setIdEquipo(string $idEquipo): self
    {
        $this->idEquipo = $idEquipo;

        return $this;
    }

    public function getIdElement(): ?Element
    {
        return $this->idElement;
    }

    public function setIdElement(?Element $idElement): self
    {
        $this->idElement = $idElement;

        return $this;
    }

    public function getIdEstat(): ?Estat
    {
        return $this->idEstat;
    }

    public function setIdEstat(?Estat $idEstat): self
    {
        $this->idEstat = $idEstat;

        return $this;
    }

    public function getIdUsuari(): ?Usuari
    {
        return $this->idUsuari;
    }

    public function setIdUsuari(?Usuari $idUsuari): self
    {
        $this->idUsuari = $idUsuari;

        return $this;
    }


}
