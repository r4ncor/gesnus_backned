<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sala
 *
 * @ORM\Table(name="sala", uniqueConstraints={@ORM\UniqueConstraint(name="uc_id_dades_seu_nom", columns={"id_dades_seu", "nom"})}, indexes={@ORM\Index(name="IDX_E226041C1F7C67A8", columns={"id_dades_seu"})})
 * @ORM\Entity
 */
class Sala
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @var \DadesSeu
     *
     * @ORM\ManyToOne(targetEntity="DadesSeu")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_dades_seu", referencedColumnName="id")
     * })
     */
    private $idDadesSeu;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getIdDadesSeu(): ?DadesSeu
    {
        return $this->idDadesSeu;
    }

    public function setIdDadesSeu(?DadesSeu $idDadesSeu): self
    {
        $this->idDadesSeu = $idDadesSeu;

        return $this;
    }


}
