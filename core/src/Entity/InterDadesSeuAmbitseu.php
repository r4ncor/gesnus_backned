<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * InterDadesSeuAmbitseu
 *
 * @ORM\Table(name="inter_dades_seu_ambitseu", indexes={@ORM\Index(name="FK_inter_dades_seu_ambitseu_usuari", columns={"id_usuari"}), @ORM\Index(name="FK_inter_dades_seu_ambitseu_dades_seu", columns={"codi_seu"}), @ORM\Index(name="FK_inter_dades_seu_ambitseu_ambit_seu", columns={"id_ambit_seu"}), @ORM\Index(name="FK_inter_dades_seu_ambitseu_estat", columns={"id_estat"})})
 * @ORM\Entity
 */
class InterDadesSeuAmbitseu
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observacions", type="text", length=0, nullable=true)
     */
    private $observacions;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="data", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $data = 'CURRENT_TIMESTAMP';

    /**
     * @var \AmbitSeu
     *
     * @ORM\ManyToOne(targetEntity="AmbitSeu")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_ambit_seu", referencedColumnName="id")
     * })
     */
    private $idAmbitSeu;

    /**
     * @var \DadesSeu
     *
     * @ORM\ManyToOne(targetEntity="DadesSeu")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="codi_seu", referencedColumnName="codi_seu")
     * })
     */
    private $codiSeu;

    /**
     * @var \Estat
     *
     * @ORM\ManyToOne(targetEntity="Estat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estat", referencedColumnName="id")
     * })
     */
    private $idEstat;

    /**
     * @var \Usuari
     *
     * @ORM\ManyToOne(targetEntity="Usuari")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuari", referencedColumnName="id")
     * })
     */
    private $idUsuari;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getObservacions(): ?string
    {
        return $this->observacions;
    }

    public function setObservacions(?string $observacions): self
    {
        $this->observacions = $observacions;

        return $this;
    }

    public function getData(): ?DateTimeInterface
    {
        return $this->data;
    }

    public function setData(DateTimeInterface $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getIdAmbitSeu(): ?AmbitSeu
    {
        return $this->idAmbitSeu;
    }

    public function setIdAmbitSeu(?AmbitSeu $idAmbitSeu): self
    {
        $this->idAmbitSeu = $idAmbitSeu;

        return $this;
    }

    public function getCodiSeu(): ?DadesSeu
    {
        return $this->codiSeu;
    }

    public function setCodiSeu(?DadesSeu $codiSeu): self
    {
        $this->codiSeu = $codiSeu;

        return $this;
    }

    public function getIdEstat(): ?Estat
    {
        return $this->idEstat;
    }

    public function setIdEstat(?Estat $idEstat): self
    {
        $this->idEstat = $idEstat;

        return $this;
    }

    public function getIdUsuari(): ?Usuari
    {
        return $this->idUsuari;
    }

    public function setIdUsuari(?Usuari $idUsuari): self
    {
        $this->idUsuari = $idUsuari;

        return $this;
    }


}
