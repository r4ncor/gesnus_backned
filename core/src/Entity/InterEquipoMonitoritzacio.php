<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * InterEquipoMonitoritzacio
 *
 * @ORM\Table(name="inter_equipo_monitoritzacio", indexes={@ORM\Index(name="FK_inter_dades_seu_estat_estat", columns={"id_monitoritzacio"}), @ORM\Index(name="FK_inter_dades_seu_estat_dades_seu", columns={"id_equipo"}), @ORM\Index(name="FK_inter_dades_seu_estat_usuari", columns={"id_usuari"})})
 * @ORM\Entity
 */
class InterEquipoMonitoritzacio
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observacions", type="text", length=0, nullable=true)
     */
    private $observacions;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="data", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $data = 'CURRENT_TIMESTAMP';

    /**
     * @var \Equipo
     *
     * @ORM\ManyToOne(targetEntity="Equipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_equipo", referencedColumnName="id")
     * })
     */
    private $idEquipo;

    /**
     * @var \Monitoritzacio
     *
     * @ORM\ManyToOne(targetEntity="Monitoritzacio")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_monitoritzacio", referencedColumnName="id")
     * })
     */
    private $idMonitoritzacio;

    /**
     * @var \Usuari
     *
     * @ORM\ManyToOne(targetEntity="Usuari")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuari", referencedColumnName="id")
     * })
     */
    private $idUsuari;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getObservacions(): ?string
    {
        return $this->observacions;
    }

    public function setObservacions(?string $observacions): self
    {
        $this->observacions = $observacions;

        return $this;
    }

    public function getData(): ?DateTimeInterface
    {
        return $this->data;
    }

    public function setData(DateTimeInterface $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getIdEquipo(): ?Equipo
    {
        return $this->idEquipo;
    }

    public function setIdEquipo(?Equipo $idEquipo): self
    {
        $this->idEquipo = $idEquipo;

        return $this;
    }

    public function getIdMonitoritzacio(): ?Monitoritzacio
    {
        return $this->idMonitoritzacio;
    }

    public function setIdMonitoritzacio(?Monitoritzacio $idMonitoritzacio): self
    {
        $this->idMonitoritzacio = $idMonitoritzacio;

        return $this;
    }

    public function getIdUsuari(): ?Usuari
    {
        return $this->idUsuari;
    }

    public function setIdUsuari(?Usuari $idUsuari): self
    {
        $this->idUsuari = $idUsuari;

        return $this;
    }


}
