<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Llicencia
 *
 * @ORM\Table(name="llicencia", uniqueConstraints={@ORM\UniqueConstraint(name="uc_nom_id_marca", columns={"nom", "id_marca"})}, indexes={@ORM\Index(name="FK_llicencia_marca", columns={"id_marca"})})
 * @ORM\Entity
 */
class Llicencia
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcio", type="text", length=0, nullable=true)
     */
    private $descripcio;

    /**
     * @var bool
     *
     * @ORM\Column(name="permanent", type="boolean", nullable=false)
     */
    private $permanent = '0';

    /**
     * @var \Marca
     *
     * @ORM\ManyToOne(targetEntity="Marca")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_marca", referencedColumnName="id")
     * })
     */
    private $idMarca;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescripcio(): ?string
    {
        return $this->descripcio;
    }

    public function setDescripcio(?string $descripcio): self
    {
        $this->descripcio = $descripcio;

        return $this;
    }

    public function getPermanent(): ?bool
    {
        return $this->permanent;
    }

    public function setPermanent(bool $permanent): self
    {
        $this->permanent = $permanent;

        return $this;
    }

    public function getIdMarca(): ?Marca
    {
        return $this->idMarca;
    }

    public function setIdMarca(?Marca $idMarca): self
    {
        $this->idMarca = $idMarca;

        return $this;
    }


}
