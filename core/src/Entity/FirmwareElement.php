<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * FirmwareElement
 *
 * @ORM\Table(name="firmware_element", uniqueConstraints={@ORM\UniqueConstraint(name="nom", columns={"nom"})})
 * @ORM\Entity
 */
class FirmwareElement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcio", type="text", length=0, nullable=true)
     */
    private $descripcio;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="data_fi", type="date", nullable=true)
     */
    private $dataFi;

    /**
     * @var bool
     *
     * @ORM\Column(name="data_no_publicada", type="boolean", nullable=false)
     */
    private $dataNoPublicada = '0';

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescripcio(): ?string
    {
        return $this->descripcio;
    }

    public function setDescripcio(?string $descripcio): self
    {
        $this->descripcio = $descripcio;

        return $this;
    }

    public function getDataFi(): ?DateTimeInterface
    {
        return $this->dataFi;
    }

    public function setDataFi(?DateTimeInterface $dataFi): self
    {
        $this->dataFi = $dataFi;

        return $this;
    }

    public function getDataNoPublicada(): ?bool
    {
        return $this->dataNoPublicada;
    }

    public function setDataNoPublicada(bool $dataNoPublicada): self
    {
        $this->dataNoPublicada = $dataNoPublicada;

        return $this;
    }


}
