<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipologiaConnexio
 *
 * @ORM\Table(name="tipologia_connexio", uniqueConstraints={@ORM\UniqueConstraint(name="uc_tipolconexio", columns={"id_criticitat", "capacitat"})}, indexes={@ORM\Index(name="FK_tipologia_conexio_ibfk_1", columns={"concepte_tarifari_seu"}), @ORM\Index(name="IDX_1D2019A7BA3737E0", columns={"id_criticitat"})})
 * @ORM\Entity
 */
class TipologiaConnexio
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="capacitat", type="string", length=255, nullable=false)
     */
    private $capacitat;

    /**
     * @var string|null
     *
     * @ORM\Column(name="unitat", type="string", length=255, nullable=true)
     */
    private $unitat;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcio_", type="text", length=0, nullable=true)
     */
    private $descripcio;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sla", type="string", length=255, nullable=true)
     */
    private $sla;

    /**
     * @var \ConcepteTarifariSeu
     *
     * @ORM\ManyToOne(targetEntity="ConcepteTarifariSeu")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="concepte_tarifari_seu", referencedColumnName="id")
     * })
     */
    private $concepteTarifariSeu;

    /**
     * @var \Criticitat
     *
     * @ORM\ManyToOne(targetEntity="Criticitat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_criticitat", referencedColumnName="id")
     * })
     */
    private $idCriticitat;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getCapacitat(): ?string
    {
        return $this->capacitat;
    }

    public function setCapacitat(string $capacitat): self
    {
        $this->capacitat = $capacitat;

        return $this;
    }

    public function getUnitat(): ?string
    {
        return $this->unitat;
    }

    public function setUnitat(?string $unitat): self
    {
        $this->unitat = $unitat;

        return $this;
    }

    public function getDescripcio(): ?string
    {
        return $this->descripcio;
    }

    public function setDescripcio(?string $descripcio): self
    {
        $this->descripcio = $descripcio;

        return $this;
    }

    public function getSla(): ?string
    {
        return $this->sla;
    }

    public function setSla(?string $sla): self
    {
        $this->sla = $sla;

        return $this;
    }

    public function getConcepteTarifariSeu(): ?ConcepteTarifariSeu
    {
        return $this->concepteTarifariSeu;
    }

    public function setConcepteTarifariSeu(?ConcepteTarifariSeu $concepteTarifariSeu): self
    {
        $this->concepteTarifariSeu = $concepteTarifariSeu;

        return $this;
    }

    public function getIdCriticitat(): ?Criticitat
    {
        return $this->idCriticitat;
    }

    public function setIdCriticitat(?Criticitat $idCriticitat): self
    {
        $this->idCriticitat = $idCriticitat;

        return $this;
    }


}
