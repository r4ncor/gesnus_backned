<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cpd
 *
 * @ORM\Table(name="cpd")
 * @ORM\Entity
 */
class Cpd
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="codi", type="string", length=255, nullable=false)
     */
    private $codi;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getCodi(): ?string
    {
        return $this->codi;
    }

    public function setCodi(string $codi): self
    {
        $this->codi = $codi;

        return $this;
    }


}
