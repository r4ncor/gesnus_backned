<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * InterEquipoAcces
 *
 * @ORM\Table(name="inter_equipo_acces", uniqueConstraints={@ORM\UniqueConstraint(name="uc_equipo_acces", columns={"id_equipo", "id_acces"})}, indexes={@ORM\Index(name="FK_inter_equipo_acces_usuari", columns={"id_usuari"}), @ORM\Index(name="FK_inter_equipo_acces_acces", columns={"id_acces"}), @ORM\Index(name="FK_inter_equipo_acces_equipo", columns={"id_equipo"})})
 * @ORM\Entity
 */
class InterEquipoAcces
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observacions", type="text", length=0, nullable=true)
     */
    private $observacions;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="data", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $data = 'CURRENT_TIMESTAMP';

    /**
     * @var \Acces
     *
     * @ORM\ManyToOne(targetEntity="Acces")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_acces", referencedColumnName="id")
     * })
     */
    private $idAcces;

    /**
     * @var \Equipo
     *
     * @ORM\ManyToOne(targetEntity="Equipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_equipo", referencedColumnName="id")
     * })
     */
    private $idEquipo;

    /**
     * @var \Usuari
     *
     * @ORM\ManyToOne(targetEntity="Usuari")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuari", referencedColumnName="id")
     * })
     */
    private $idUsuari;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getObservacions(): ?string
    {
        return $this->observacions;
    }

    public function setObservacions(?string $observacions): self
    {
        $this->observacions = $observacions;

        return $this;
    }

    public function getData(): ?DateTimeInterface
    {
        return $this->data;
    }

    public function setData(?DateTimeInterface $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getIdAcces(): ?Acces
    {
        return $this->idAcces;
    }

    public function setIdAcces(?Acces $idAcces): self
    {
        $this->idAcces = $idAcces;

        return $this;
    }

    public function getIdEquipo(): ?Equipo
    {
        return $this->idEquipo;
    }

    public function setIdEquipo(?Equipo $idEquipo): self
    {
        $this->idEquipo = $idEquipo;

        return $this;
    }

    public function getIdUsuari(): ?Usuari
    {
        return $this->idUsuari;
    }

    public function setIdUsuari(?Usuari $idUsuari): self
    {
        $this->idUsuari = $idUsuari;

        return $this;
    }


}
