<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * InterTipologiaConnexioEstat
 *
 * @ORM\Table(name="inter_tipologia_connexio_estat", indexes={@ORM\Index(name="FK_inter_tipologia_connexio_estat_estat", columns={"id_estat"}), @ORM\Index(name="FK_inter_tipologia_connexio_estat_tipologia_connexio", columns={"id_tipologia_connexio"}), @ORM\Index(name="FK_inter_tipologia_connexio_estat_usuari", columns={"id_usuari"})})
 * @ORM\Entity
 */
class InterTipologiaConnexioEstat
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="observacions", type="text", length=0, nullable=true)
     */
    private $observacions;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="data", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $data = 'CURRENT_TIMESTAMP';

    /**
     * @var \Estat
     *
     * @ORM\ManyToOne(targetEntity="Estat")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estat", referencedColumnName="id")
     * })
     */
    private $idEstat;

    /**
     * @var \TipologiaConnexio
     *
     * @ORM\ManyToOne(targetEntity="TipologiaConnexio")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipologia_connexio", referencedColumnName="id")
     * })
     */
    private $idTipologiaConnexio;

    /**
     * @var \Usuari
     *
     * @ORM\ManyToOne(targetEntity="Usuari")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuari", referencedColumnName="id")
     * })
     */
    private $idUsuari;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getObservacions(): ?string
    {
        return $this->observacions;
    }

    public function setObservacions(?string $observacions): self
    {
        $this->observacions = $observacions;

        return $this;
    }

    public function getData(): ?DateTimeInterface
    {
        return $this->data;
    }

    public function setData(DateTimeInterface $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getIdEstat(): ?Estat
    {
        return $this->idEstat;
    }

    public function setIdEstat(?Estat $idEstat): self
    {
        $this->idEstat = $idEstat;

        return $this;
    }

    public function getIdTipologiaConnexio(): ?TipologiaConnexio
    {
        return $this->idTipologiaConnexio;
    }

    public function setIdTipologiaConnexio(?TipologiaConnexio $idTipologiaConnexio): self
    {
        $this->idTipologiaConnexio = $idTipologiaConnexio;

        return $this;
    }

    public function getIdUsuari(): ?Usuari
    {
        return $this->idUsuari;
    }

    public function setIdUsuari(?Usuari $idUsuari): self
    {
        $this->idUsuari = $idUsuari;

        return $this;
    }


}
