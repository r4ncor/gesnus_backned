<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Monitoritzacio
 *
 * @ORM\Table(name="monitoritzacio", uniqueConstraints={@ORM\UniqueConstraint(name="uc_nom", columns={"nom"})})
 * @ORM\Entity
 */
class Monitoritzacio
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcio", type="string", length=255, nullable=false)
     */
    private $descripcio;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescripcio(): ?string
    {
        return $this->descripcio;
    }

    public function setDescripcio(string $descripcio): self
    {
        $this->descripcio = $descripcio;

        return $this;
    }


}
