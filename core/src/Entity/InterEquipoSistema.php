<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * InterEquipoSistema
 *
 * @ORM\Table(name="inter_equipo_sistema", uniqueConstraints={@ORM\UniqueConstraint(name="uc_sistema", columns={"id_equipo", "id_sistema"})}, indexes={@ORM\Index(name="FK_equipo_sistema_equipo", columns={"id_equipo"}), @ORM\Index(name="FK_equipo_sistema_sistema", columns={"id_sistema"})})
 * @ORM\Entity
 */
class InterEquipoSistema
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="data", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $data = 'CURRENT_TIMESTAMP';

    /**
     * @var \Equipo
     *
     * @ORM\ManyToOne(targetEntity="Equipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_equipo", referencedColumnName="id")
     * })
     */
    private $idEquipo;

    /**
     * @var \Sistema
     *
     * @ORM\ManyToOne(targetEntity="Sistema")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_sistema", referencedColumnName="id")
     * })
     */
    private $idSistema;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getData(): ?DateTimeInterface
    {
        return $this->data;
    }

    public function setData(DateTimeInterface $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getIdEquipo(): ?Equipo
    {
        return $this->idEquipo;
    }

    public function setIdEquipo(?Equipo $idEquipo): self
    {
        $this->idEquipo = $idEquipo;

        return $this;
    }

    public function getIdSistema(): ?Sistema
    {
        return $this->idSistema;
    }

    public function setIdSistema(?Sistema $idSistema): self
    {
        $this->idSistema = $idSistema;

        return $this;
    }


}
