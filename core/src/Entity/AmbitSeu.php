<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AmbitSeu
 *
 * @ORM\Table(name="ambit_seu", uniqueConstraints={@ORM\UniqueConstraint(name="vrf", columns={"vrf"})})
 * @ORM\Entity
 */
class AmbitSeu
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcio", type="text", length=0, nullable=true)
     */
    private $descripcio;

    /**
     * @var string
     *
     * @ORM\Column(name="vrf", type="string", length=255, nullable=false)
     */
    private $vrf;

    /**
     * @var bool
     *
     * @ORM\Column(name="actiu", type="boolean", nullable=false, options={"default"="1"})
     */
    private $actiu = '1';

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescripcio(): ?string
    {
        return $this->descripcio;
    }

    public function setDescripcio(?string $descripcio): self
    {
        $this->descripcio = $descripcio;

        return $this;
    }

    public function getVrf(): ?string
    {
        return $this->vrf;
    }

    public function setVrf(string $vrf): self
    {
        $this->vrf = $vrf;

        return $this;
    }

    public function getActiu(): ?bool
    {
        return $this->actiu;
    }

    public function setActiu(bool $actiu): self
    {
        $this->actiu = $actiu;

        return $this;
    }


}
