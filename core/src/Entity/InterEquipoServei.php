<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * InterEquipoServei
 *
 * @ORM\Table(name="inter_equipo_servei", uniqueConstraints={@ORM\UniqueConstraint(name="uc_id_equipo_id_estat_data", columns={"id_equipo", "id_servei", "data"})}, indexes={@ORM\Index(name="FK_inter_equipo_estat_equipo", columns={"id_equipo"}), @ORM\Index(name="FK_inter_equipo_estat_servei", columns={"id_servei"}), @ORM\Index(name="FK_inter_equipo_estat_usuari", columns={"id_usuari"})})
 * @ORM\Entity
 */
class InterEquipoServei
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="data", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $data = 'CURRENT_TIMESTAMP';

    /**
     * @var \Servei
     *
     * @ORM\ManyToOne(targetEntity="Servei")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_servei", referencedColumnName="id")
     * })
     */
    private $idServei;

    /**
     * @var \Equipo
     *
     * @ORM\ManyToOne(targetEntity="Equipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_equipo", referencedColumnName="id")
     * })
     */
    private $idEquipo;

    /**
     * @var \Usuari
     *
     * @ORM\ManyToOne(targetEntity="Usuari")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuari", referencedColumnName="id")
     * })
     */
    private $idUsuari;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getData(): ?DateTimeInterface
    {
        return $this->data;
    }

    public function setData(DateTimeInterface $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getIdServei(): ?Servei
    {
        return $this->idServei;
    }

    public function setIdServei(?Servei $idServei): self
    {
        $this->idServei = $idServei;

        return $this;
    }

    public function getIdEquipo(): ?Equipo
    {
        return $this->idEquipo;
    }

    public function setIdEquipo(?Equipo $idEquipo): self
    {
        $this->idEquipo = $idEquipo;

        return $this;
    }

    public function getIdUsuari(): ?Usuari
    {
        return $this->idUsuari;
    }

    public function setIdUsuari(?Usuari $idUsuari): self
    {
        $this->idUsuari = $idUsuari;

        return $this;
    }


}
