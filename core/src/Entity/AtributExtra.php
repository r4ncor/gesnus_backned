<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AtributExtra
 *
 * @ORM\Table(name="atribut_extra", uniqueConstraints={@ORM\UniqueConstraint(name="uc_identificador", columns={"identificador"})})
 * @ORM\Entity
 */
class AtributExtra
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nom", type="text", length=0, nullable=true)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="identificador", type="string", length=255, nullable=false)
     */
    private $identificador;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getIdentificador(): ?string
    {
        return $this->identificador;
    }

    public function setIdentificador(string $identificador): self
    {
        $this->identificador = $identificador;

        return $this;
    }


}
