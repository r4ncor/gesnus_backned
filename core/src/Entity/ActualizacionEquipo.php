<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * ActualizacionEquipo
 *
 * @ORM\Table(name="actualizacion_equipo", indexes={@ORM\Index(name="FK_actualizacion_equipo_model", columns={"id_model"}), @ORM\Index(name="FK_actualizacion_equipo_firmware", columns={"id_firmware"})})
 * @ORM\Entity
 */
class ActualizacionEquipo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="serial_number", type="string", length=255, nullable=true)
     */
    private $serialNumber;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hostname", type="string", length=255, nullable=true)
     */
    private $hostname;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="responde", type="boolean", nullable=true, options={"default"="1"})
     */
    private $responde = '1';

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="ultima_actualizacion", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $ultimaActualizacion = 'CURRENT_TIMESTAMP';

    /**
     * @var \Firmware
     *
     * @ORM\ManyToOne(targetEntity="Firmware")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_firmware", referencedColumnName="id")
     * })
     */
    private $idFirmware;

    /**
     * @var \Model
     *
     * @ORM\ManyToOne(targetEntity="Model")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_model", referencedColumnName="id")
     * })
     */
    private $idModel;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getSerialNumber(): ?string
    {
        return $this->serialNumber;
    }

    public function setSerialNumber(?string $serialNumber): self
    {
        $this->serialNumber = $serialNumber;

        return $this;
    }

    public function getHostname(): ?string
    {
        return $this->hostname;
    }

    public function setHostname(?string $hostname): self
    {
        $this->hostname = $hostname;

        return $this;
    }

    public function getResponde(): ?bool
    {
        return $this->responde;
    }

    public function setResponde(?bool $responde): self
    {
        $this->responde = $responde;

        return $this;
    }

    public function getUltimaActualizacion(): ?DateTimeInterface
    {
        return $this->ultimaActualizacion;
    }

    public function setUltimaActualizacion(?DateTimeInterface $ultimaActualizacion): self
    {
        $this->ultimaActualizacion = $ultimaActualizacion;

        return $this;
    }

    public function getIdFirmware(): ?Firmware
    {
        return $this->idFirmware;
    }

    public function setIdFirmware(?Firmware $idFirmware): self
    {
        $this->idFirmware = $idFirmware;

        return $this;
    }

    public function getIdModel(): ?Model
    {
        return $this->idModel;
    }

    public function setIdModel(?Model $idModel): self
    {
        $this->idModel = $idModel;

        return $this;
    }


}
