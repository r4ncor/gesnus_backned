#!/bin/bash

### Los parámetros vienen definidos desde el script task.sh ejecutado por Jenkins
ENTORNO="${1}"
RELEASE="${2}"
PROJECT_NAME="${3}"
BUILD_USER="${4}"
DEPLOYING_USER="${5}"
MAX_RELEASES=1

### Función para ejecutar una orden forzando una salida en caso de error
function eval_checked
{
    echo "Evaluando: $1"
    eval $1

    if [ $? -ne 0 ]
    then
        exit 1
    fi
}

### Función para eliminar todos los archivos del directorio actual excepto los -n últimos indicados
function clear_releases
{
    eval_checked "cd \"${HTML_PATH}${PROJECT_LINK}_releases/\""

    total_files=$(ls -1|wc -l)

    if [ ${total_files} -gt ${MAX_RELEASES} ]
    then
        echo "Eliminando releases (${MAX_RELEASES} de ${total_files})"
        eval_checked "rm -Rf $(ls|head -n -${MAX_RELEASES})"
    fi
}

case ${ENTORNO} in
    'PRE')
#        HTML_PATH="/var/www/html/intranetfe/"
#        PROJECT_LINK="${PROJECT_NAME}"
        ;;
    'PRO')
        HTML_PATH="/var/www/html/"
        PROJECT_LINK="${PROJECT_NAME}"
        ;;
    *)
        ;;
esac

cd ${HTML_PATH}

### -p crea todos los directorios incluidos en el path si no existen
echo "Creando los directorios incluidos en el path si no existen"
eval_checked "mkdir -p \"${HTML_PATH}${PROJECT_LINK}_releases/${RELEASE}\""
eval_checked "cd \"${HTML_PATH}${PROJECT_LINK}_releases/${RELEASE}\""

### Descomprime el archivo comprimido que contiene el despliegue en el directorio creado previamente,
### manteniendo los permisos originales
echo "Descomprimiendo el archivo comprimido"
eval_checked "tar --preserve-permissions -xzf /tmp/${PROJECT_NAME}Jenkins.tar.gz --warning=no-timestamp"

### Damos permisos al usuario
echo "Dando permisos al usuario de despliegue \"${DEPLOYING_USER}\" para la release creada"
eval_checked "setfacl -R -m u:${DEPLOYING_USER}:rw \"${HTML_PATH}${PROJECT_LINK}_releases/${RELEASE}\""

### Una vez descomprimido borramos el archvio comprimido
echo "Eliminando el archivo comprimido"
eval_checked "rm /tmp/${PROJECT_NAME}Jenkins.tar.gz"

###
eval_checked "composer install &> /dev/null"

### Ejecución del comando updateapp para la actualizacion del la bd
### php yii init-app/run
