#!/bin/bash

### Los parámetros vienen definidos desde el script task.sh ejecutado por Jenkins
ENTORNO="${1}"
RELEASE="${2}"
PROJECT_NAME="${3}"
DEPLOYING_USER="${4}"
MAX_RELEASES=1

### Función para ejecutar una orden forzando una salida en caso de error
function eval_checked
{
    echo "Evaluando: $1"
    eval $1

    if [ $? -ne 0 ]
    then
        exit 1
    fi
}

function clear_releases
{
    eval_checked "cd \"${HTML_PATH}${PROJECT_LINK}_releases/\""

    total_files=$(ls -1|wc -l)

    if [[ ${total_files} -gt ${MAX_RELEASES} ]]
    then
        echo "Eliminando releases (${MAX_RELEASES} de ${total_files})"
        echo "rm -Rf $(ls|head -n -${MAX_RELEASES})"
        eval_checked "rm -Rf $(ls|head -n -${MAX_RELEASES})"
    fi
}

case ${ENTORNO} in
    'PRE')
#        HTML_PATH="/var/www/html/intranetfe/"
#        PROJECT_LINK="${PROJECT_NAME}"
        ;;
    'PRO')
        HTML_PATH="/var/www/html/"
        PROJECT_LINK="${PROJECT_NAME}"
        ;;
    *)
        ;;
esac

cd ${HTML_PATH}

echo "Eliminando el enlace simbólico"
eval_checked "rm -f cecheckout"
eval_checked "rm -f cecheckout_core"
echo "Creando nuevo enlace simbólico"
eval_checked "ln -s \"${HTML_PATH}${PROJECT_LINK}_releases/${RELEASE}/public\" cecheckout_core"
eval_checked "ln -s \"${HTML_PATH}${PROJECT_LINK}_releases/${RELEASE}\" cecheckout"
echo "Reiniciando Supervisor"



### Borramos todas las releases del directorio excepto las -n últimas
clear_releases