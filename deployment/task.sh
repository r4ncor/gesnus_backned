#!/bin/bash

### Los parámetros vienen definidos por las opciones escogidas en el entorno
ENTORNO="${1}"
BUILD_USER="${2}"
BRANCH="${3}"
RELEASE=`date "+%Y%m%d%H%M%S"`
PROJECT_NAME="cecheckout"

### Función para ejecutar una orden forzando una salida en caso de error
function eval_checked
{
    echo "Evaluando: $1"
    eval $1

    if [ $? -ne 0 ]
    then
        exit 1
    fi
}


case ${ENTORNO} in
    'PRE')
        ;;
    'PRO')
        ;;
    *)
        echo "Error, no se ha seleccionado entorno de despliegue."
   ;;
esac

### Se eliminan los ficheros de configuracion de los otros entornos y se renombra el del entorno actual a main.php
ENT=`echo ${CONFIG} | awk '{print tolower($0)}'`

### Entramos en el directorio core para realizar la descarga de Composer
cd core

### Generamos el archivo "version"
eval_checked "echo ${BRANCH} | cut -f2 -d'/' > version"

### Esta línea construye el archivo comprimido de la rama descargada
echo "Comprimiendo rama para despliegue."
eval_checked "tar --preserve-permissions -czf ../deployment/${PROJECT_NAME}Jenkins.tar.gz ./"

### Salimos de core para realizar la tranferencia de capetas y archivo de inicialización
cd ..

## En caso de que sea el entorno de producción, se deployará en los demás frontales
for HOST in "${DEPLOYING_HOST[@]}"
do
    echo " "
    echo "----------------------------------------------------------"
    echo "Deployando en el host: ${HOST}"
    echo "----------------------------------------------------------"
    echo " "
    echo "Copiando fichero 'initialize.sh' al frontal ${HOST}"
    eval_checked "scp deployment/initialize.sh ${DEPLOYING_USER}@${HOST}:/tmp/initialize_${PROJECT_NAME}.sh;"

    echo "Copiando fichero comprimido al frontal ${HOST}"
    eval_checked "scp deployment/${PROJECT_NAME}Jenkins.tar.gz ${DEPLOYING_USER}@${HOST}:/tmp/"

    echo "Ejecutando el script initialize_${PROJECT_NAME}.sh con permisos chmod 700."
    eval_checked "ssh ${DEPLOYING_USER}@${HOST} \"chmod 700 /tmp/initialize_${PROJECT_NAME}.sh; bash /tmp/initialize_${PROJECT_NAME}.sh ${ENTORNO} ${RELEASE} ${PROJECT_NAME} ${BUILD_USER} ${DEPLOYING_USER}\""

    echo "Eliminando el script initialize_${PROJECT_NAME}.sh del servidor que estamos desplegando."
    eval_checked "ssh ${DEPLOYING_USER}@${HOST} \"rm -f /tmp/initialize_${PROJECT_NAME}.sh\""
done


for HOST in "${DEPLOYING_HOST[@]}"
do
   echo " "
   echo "----------------------------------------------------------"
   echo "Cambiando el enlace simbolico: ${HOST}"
   echo "----------------------------------------------------------"
   echo " "

   echo "Copiando fichero 'releases.sh' al frontal ${HOST}"
   eval_checked "scp deployment/releases.sh ${DEPLOYING_USER}@${HOST}:/tmp/releases.sh;"

   echo "Ejecutando el script initialize_${PROJECT_NAME}.sh con permisos chmod 700."
   eval_checked "ssh ${DEPLOYING_USER}@${HOST} \"chmod 700 /tmp/releases.sh; bash /tmp/releases.sh ${ENTORNO} ${RELEASE} ${PROJECT_NAME} ${DEPLOYING_USER}\""

done

echo " "
echo "----------------------------------------------------------"
echo "Fin deployment"
echo "----------------------------------------------------------"
echo " "