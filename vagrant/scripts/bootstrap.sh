#!/usr/bin/env bash

sudo yum -y update

sudo yum -y install http://rpms.remirepo.net/enterprise/remi-release-7.rpm
sudo yum -y install epel-release yum-utils

sudo yum-config-manager --disable remi-php54
sudo yum-config-manager --enable remi-php73

sudo yum -y install php php-cli php-fpm php-xdebug php-mysqlnd php-zip php-devel php-gd php-mcrypt php-mbstring php-curl php-xml php-pear php-bcmath php-json nano vim git unzip php-ldap php-soap php-ssh2 php-mssql wget

cat >/etc/yum.repos.d/nginx.repo <<EOF
[nginx-stable]
name=nginx stable repo
baseurl=http://nginx.org/packages/centos/\$releasever/\$basearch/
gpgcheck=1
enabled=1
gpgkey=https://nginx.org/keys/nginx_signing.key


[nginx-mainline]
name=nginx mainline repo
baseurl=http://nginx.org/packages/mainline/centos/\$releasever/\$basearch/
gpgcheck=1
enabled=0
gpgkey=https://nginx.org/keys/nginx_signing.key
EOF

sudo yum-config-manager --enable nginx-mainline
sudo yum install -y nginx

sudo systemctl start nginx
sudo systemctl enable nginx

cat >/etc/nginx/conf.d/default.conf <<EOF
server {
    charset utf-8;
    client_max_body_size 128M;

    listen 80;

    server_name $2;
    root        $1;
    index       index.php;

    access_log  /var/log/nginx/access.log;
    error_log   /var/log/nginx/error.log;

    location / {
        # Redirect everything that isn't a real file to index.php
        try_files \$uri \$uri/ /index.php\$is_args\$args;
    }

    # deny accessing php files for the /assets directory
    location ~ ^/assets/.*\.php\$ {
        deny all;
    }

    location ~ \.php\$ {
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
        fastcgi_pass 127.0.0.1:9000;
        try_files \$uri =404;
    }

    location ~* /\. {
        deny all;
    }
}
EOF

# Creating the configuration for Xdebug

#cat > /etc/php5/apache2/conf.d/20-xdebug.ini << EOF
cat > /etc/php.d/15-xdebug.ini << EOF
zend_extension=xdebug.so
xdebug.default_enable=1
xdebug.remote_enable=1
xdebug.remote_handler=dbgp
xdebug.remote_connect_back=1
xdebug.remote_port=9000
xdebug.remote_autostart=1
xdebug.remote_log=/var/www/html/xdebug.log
EOF

sudo sed -i 's/user  nginx;/user  vagrant;/' /etc/nginx/nginx.conf
sudo sed -i 's/user = apache/user = vagrant/' /etc/php-fpm.d/www.conf
sudo sed -i 's/group = apache/group = vagrant/' /etc/php-fpm.d/www.conf
sudo sed -i 's/;listen.owner = nobody/listen.owner = vagrant/' /etc/php-fpm.d/www.conf
sudo sed -i 's/;listen.group = nobody/listen.group = vagrant/' /etc/php-fpm.d/www.conf

sudo chmod 777 /var/lib/php/session -R

sudo systemctl restart nginx.service
sudo systemctl restart php-fpm.service

sudo php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer

su vagrant
/usr/local/bin/composer config -g github-oauth.github.com a6acf7877fd5cc1bb41eea0019d410a6b0e7dc7c
/usr/local/bin/composer global require "fxp/composer-asset-plugin:*"
